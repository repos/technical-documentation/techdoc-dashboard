(defproject techdoc.dashboard "1.0.3"
  :description "Technical documentation dashboard"
  :license {:name "Eclipse Public License - v 2.0"}
  :profiles {:uberjar {:main techdoc.dashboard.core, :aot :all}}
  :dependencies [[org.clojure/clojure             "1.12.0"]
                 [metosin/reitit                  "0.7.2"]
                 [metosin/ring-http-response      "0.9.4"]
                 [ring/ring-core                  "1.13.0"]
                 [ring/ring-defaults              "0.5.0"]
                 [ch.qos.logback/logback-classic  "1.5.12"]
                 [luminus-transit/luminus-transit "0.1.6" :exclusions [com.cognitect/transit-clj]]
                 [metosin/muuntaja                "0.6.10"]
                 [io.github.kit-clj/kit-core "1.0.9"]
                 [io.github.kit-clj/kit-undertow "1.0.7"]
                 [io.github.kit-clj/kit-repl "1.0.3"]
                 [cheshire/cheshire "5.13.0"]
                 [clj-http/clj-http "3.13.0"]
                 [org.clojure/core.cache "1.1.234"]
                 [tongue/tongue "0.4.4"]
                 [throttler/throttler "1.0.1"]
                 [clojure.java-time/clojure.java-time "1.4.3"]
                 [com.github.seancorfield/honeysql "2.6.1243"]
                 [dali/dali "1.0.2"]
                 [org.clojure/tools.reader "1.5.0"]
                 [hiccup "2.0.0-RC4"]
                 [org.mariadb.jdbc/mariadb-java-client "3.5.1"]
                 [com.github.seancorfield/next.jdbc "1.3.981"]]
  :uberjar-name "dashboard-standalone.jar"
  :main techdoc.dashboard.core
  :aot [techdoc.dashboard.core]
  :source-paths ["src/clj" "env/prod/clj"]
  :resource-paths ["resources"]
  :min-lein-version "2.0.0")
