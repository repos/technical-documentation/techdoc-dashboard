(ns techdoc.dashboard.dashboardpage-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.web.ui.tables :as tables]
    [techdoc.dashboard.web.ui.widgets :as widgets]))


(set! *warn-on-reflection* true)


(deftest table
  (is (= :table#pageviews-table
         (first (flatten (tables/table-for-pageviews [{:date "2023052500" :value 1}] "en" "1" "1")))))
  (is (.equals "20230525"
               (some #{"20230525"} (flatten (tables/table-for-pageviews [{:date "2023052500" :value 1}] "en" "1" "1")))))
  (is (= :table#pageedits-table
         (first (flatten (tables/table-for-edits [{:date "2023052500" :value 9}] "en" "1" "1")))))
  (is (.equals "9"
               (last (flatten (tables/table-for-edits [{:date "2023062500" :value 9}] "en" "1" "1")))))
  (is (= :table#template-data-table
         (first (flatten (tables/table-for-templates [{:page "Test" :template "Test"}] "en" "1" "1")))))
  (is (.equals "Test"
               (last (flatten (tables/table-for-templates [{:page "Test" :template "Test"}] "en" "1" "1")))))
  (is (= :table#translate-data-table
         (first (flatten (tables/table-for-translate-data [{:page "Test" :translate true}] "en" "1" "1")))))
  (is (.equals "No"
               (last (flatten (tables/table-for-translate-data [{:page "Test" :translate false}] "en" "1" "1")))))
  (is (= :table#editors-table
         (first (flatten (tables/table-for-editors [{:editor "Test" :edits 1}] "en" "1" "1")))))
  (is (= 2
         (last (flatten (tables/table-for-editors [{:editor "Test" :edits 2}] "en" "1" "1"))))))


(deftest widget
  (is (= 36
         (count (flatten (widgets/pageviews-widget "" nil false "en" "1" "1")))))
  (is (= 44
         (count (flatten (widgets/pageviews-widget "" [{:date "2023082400" :value 1}] false "en" "1" "1")))))
  (is (= 48
         (count (flatten (widgets/pageviews-widget "" [{:date "2023082400" :value 1}] true "en" "1" "1")))))
  (is (= :div.chart-box
         (some #{:div.chart-box} (flatten (widgets/pageviews-widget "" [{:date "2023082400" :value 1}] true "en" "1" "1")))))
  (is (nil? (some #{:div.chart-box} (flatten (widgets/pageviews-widget "" [{:date "2023082400", :value 1}] false "en" "1" "1")))))
  (is (= 41
         (count (flatten (widgets/templates-widget "Test" [{:page "Test" :template "Test"}] "en" "1" "1"))))))
