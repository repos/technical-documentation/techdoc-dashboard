(ns techdoc.dashboard.chartgenerator-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.lib.chart-generator :as cg]))


(set! *warn-on-reflection* true)


(def chart
  (cg/generate-bar-chart (vec (range 1 91)) "90 values" "Value" "Date"))


(def chart-svg
  (cg/chart->svg chart))


(def sizes-chart
  (cg/generate-sizes-chart
    {:big_edits 1
     :small_edits 2
     :minor_edits 3
     :major_edits 4}
    "Title"
    "Number of edits"
    "Big edits"
    "Small edits"
    "Major edits"
    "Minor edits"))


(def sizeschart-svg
  (cg/chart->svg sizes-chart))


(deftest generate-chart
  (is (= :dali/page
         (first (flatten chart))))
  (is (= 224
         (last (flatten chart))))
  (is (= :dali/page
         (first (flatten sizes-chart))))
  (is (= 224
         (last (flatten sizes-chart)))))


(deftest chart-to-svg
  (is (.equals "<svg xmlns=\"http://www.w3.org/2000/svg\""
               (re-find #"<svg xmlns=\"http://www.w3.org/2000/svg\"" chart-svg)))
  (is (.equals "</svg>"
               (re-find #"</svg>" chart-svg)))
  (is (.equals "<svg xmlns=\"http://www.w3.org/2000/svg\""
               (re-find #"<svg xmlns=\"http://www.w3.org/2000/svg\"" sizeschart-svg)))
  (is (.equals "</svg>"
               (re-find #"</svg>" sizeschart-svg))))
