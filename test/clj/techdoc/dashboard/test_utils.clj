(ns techdoc.dashboard.test-utils
  (:require
    [integrant.repl.state :as state]
    [techdoc.dashboard.core :as core]))


(set! *warn-on-reflection* true)


(defn system-state
  []
  (or @core/system state/system))


(defn system-fixture
  []
  (fn [f]
    (when (nil? (system-state))
      (core/start-app {:opts {:profile :test}}))
    (f)))
