(ns techdoc.dashboard.pagepile-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.web.controllers.cache :as cache]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]))


(set! *warn-on-reflection* true)


(def test-data
  (read-string (slurp "test/clj/techdoc/dashboard/test_data.edn"))) ; load PagePile error responses from test-data.edn

(def correct-pagepile
  (pagepile/load-pagepile-through-cache 47924)) ; load correct PagePile directly from API and cache it

(def cached-correct-pagepile
  (cache/just-fetch-record (keyword "pagepile" (str 47924)))) ; load cached record

(def loaded-correct-pagepile
  (pagepile/load-pagepile-from-cache 47924)) ; load cached record using wrapper

(def loaded-incorrect-pagepile
  (pagepile/load-pagepile-from-cache 99)) ; attempt to load pagepile that doesn't exist in cache

(def incorrect-pagepile
  (:incorrect test-data))


(def forbidden-pagepile
  (:forbidden test-data))


(def connection-error
  (:not-connected test-data))


(deftest validations
  (is (= :ok
         (:status (pagepile/validate-pagepile correct-pagepile :en))))
  (is (.equals "wikitechwikitechwikimedia"
               (:wiki (pagepile/validate-pagepile correct-pagepile :en))))
  (is (.equals "Invalid PagePile ID"
               (:body (pagepile/validate-pagepile incorrect-pagepile :en))))
  (is (.equals "Cannot connect to PagePile"
               (:body (pagepile/validate-pagepile connection-error :en))))
  (is (.equals "PagePile contains pages from an unsupported wiki"
               (:body (pagepile/validate-pagepile forbidden-pagepile :en)))))


(deftest cache
  (is (= :ok
         (:status (pagepile/validate-pagepile cached-correct-pagepile :en))))
  (is (= :ok
         (:status (pagepile/validate-pagepile loaded-correct-pagepile :en))))
  (is (= :error
         (:status (pagepile/validate-pagepile loaded-incorrect-pagepile :en)))))


(deftest getters
  (is (true? (:error (pagepile/get-pagepile-structured "4999" :en true))))
  (is (false? (:error (pagepile/get-pagepile-structured "47924" :en true)))))
