(ns techdoc.dashboard.mwclient-test
  (:require
    [clojure.test :refer :all]
    [java-time.api :as jt]
    [techdoc.dashboard.lib.common :as common]
    [techdoc.dashboard.lib.mw-client :as mw]))


(set! *warn-on-reflection* true)


(deftest dates-structure
  (is (= 90 ; dates structure should contain 90 days
         (count (common/dates-structure)))
      (nil? (get (common/dates-structure) (keyword (str (common/UTC-date-now) "00")))))
  (is (zero?
        (get
          (common/dates-structure)
          (keyword
            (jt/format
              "yyyyMMdd00"
              (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 1))))))))


(deftest calculate-sum
  (is (= 2
         (:one (common/calculate-sum [{:one 1} {:one 1}]))))
  (is (zero? (count (common/calculate-sum [])))))


(deftest pageviews-for-collection
  (is (= 90
         (count (mw/get-pageviews-for-collection ["MediaWiki"] "mediawikiwiki"))))
  (is (<= 1
          (:value (first (mw/get-pageviews-for-collection ["MediaWiki"] "mediawikiwiki"))))))


(deftest pageviews-for-page
  (is (= 90
         (count (mw/get-pageviews-for-page-by-cache-id (hash (str "mediawikiwiki" "MediaWiki")) "mediawikiwiki"))))
  (is (<= 1
          (:value (first (mw/get-pageviews-for-page-by-cache-id (hash (str "mediawikiwiki" "MediaWiki"))  "mediawikiwiki"))))))


(deftest page-translation-data
  (let [correct-page (first (mw/get-translate-data ["Template:Main_page"] "mediawikiwiki"))
        incorrect-page (first (mw/get-translate-data ["PageThatMostLikelyDoesNotExist"] "mediawikiwiki"))
        response-without-translate {:body {:query {:pages [{:title "without-translate" :revisions [{:slots {:main {:content "Content without translations"}}}]}]}}}
        response-with-translate {:body {:query {:pages [{:title "with-translate" :revisions [{:slots {:main {:content "Content with <translate>"}}}]}]}}}]
    (is (true? (:translate correct-page)))
    (is (nil? (:translate incorrect-page)))
    (is (true?
          (:translate (first (mw/normalize-translate-data response-with-translate)))))
    (is (false?
          (:translate (first (mw/normalize-translate-data response-without-translate)))))))
