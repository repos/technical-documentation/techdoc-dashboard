(ns techdoc.dashboard.htmx-test
  (:require
    [clojure.string :as str]
    [clojure.test :refer :all]
    [techdoc.dashboard.web.htmx :as htmx]))


(set! *warn-on-reflection* true)


(deftest htmx-page
  (is (= 200
         (:status (htmx/page :en "htmx-test"))))
  (is (true? (str/includes? (:body (htmx/page :en "htmx-test")) "htmx.min.js")))
  (is (false? (str/includes? (:body (htmx/page :en "htmx-test")) "non-existent text")))
  (is (true?
        (str/includes? (:body (htmx/page :en "htmx-test")) "Original content available under the terms")))
  (is (true?
        (str/includes? (:body (htmx/page :pl "htmx-test")) "repozytorium z kodem"))))


(deftest htmx-ui
  (is (.equals "test"
               (:body (htmx/ui "test"))))
  (is (false? (str/includes? (:body (htmx/ui "test")) "non-existent text")))
  (is (= 200
         (:status (htmx/ui "test")))))
