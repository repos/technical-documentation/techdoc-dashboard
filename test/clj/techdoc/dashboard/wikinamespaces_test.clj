(ns techdoc.dashboard.wikinamespaces-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.lib.wiki-namespaces :as wn]))


(set! *warn-on-reflection* true)


(deftest wn-test
  (let [wn (wn/get-wiki-namespaces "wikitechwikimedia")]
    (is (zero? (get wn nil)))
    (is (= 117
           (get wn "Tool talk")))
    (is (nil? (get wn "Namespace that does not exist")))))
