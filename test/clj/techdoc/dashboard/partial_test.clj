(ns techdoc.dashboard.partial-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.ui.partial :as partial]))


(set! *warn-on-reflection* true)
(pagepile/load-pagepile-through-cache 59125)


(def correct-parameters
  {:path-params {:pagepile-id "59125"
                 :element-id "page-views"}
   :params {:lang "en"
            :col "value"
            :ord "desc"
            :cid "59125"}})


(def incorrect-pagepile
  {:path-params {:pagepile-id "incorrect"
                 :element-id "page-views"}
   :params {:lang "en"
            :col "value"
            :ord "desc"
            :cid "59125"}})


(def incorrect-element-id
  {:path-params {:pagepile-id "59125"
                 :element-id "incorrect"}
   :params {:lang "en"
            :col "value"
            :ord "desc"
            :cid "59125"}})


(def incorrect-lang
  {:path-params {:pagepile-id "59125"
                 :element-id "page-views"}
   :params {:lang "incorrect"
            :col "value"
            :ord "desc"
            :cid "59125"}})


(def incorrect-col
  {:path-params {:pagepile-id "59125"
                 :element-id "page-views"}
   :params {:lang "en"
            :col "incorrect"
            :ord "desc"
            :cid "59125"}})


(def incorrect-ord
  {:path-params {:pagepile-id "59125"
                 :element-id "page-views"}
   :params {:lang "en"
            :col "value"
            :ord "incorrect"
            :cid "59125"}})


(def incorrect-cid
  {:path-params {:pagepile-id "59125"
                 :element-id "page-views"}
   :params {:lang "en"
            :col "value"
            :ord "desc"
            :cid "incorrect"}})


(deftest validation
  (is (true? (partial/incorrect-parameter? "test" partial/col-values)))
  (is (false? (partial/incorrect-parameter? "editor" partial/col-values)))
  (is (true? (partial/incorrect-parameter? "test" partial/ord-values)))
  (is (false? (partial/incorrect-parameter? "desc" partial/ord-values)))
  (is (true? (partial/incorrect-number? "a")))
  (is (false? (partial/incorrect-number? "-1")))
  (is (false? (partial/incorrect-number? "1"))))


(deftest get-element
  (is (true?
        (-> (partial/get-element correct-parameters) (get :body) (clojure.string/includes? "table"))))
  (is (false?
        (-> (partial/get-element incorrect-pagepile) (get :body) (clojure.string/includes? "table"))))
  (is (false?
        (-> (partial/get-element incorrect-element-id) (get :body) (clojure.string/includes? "table"))))
  (is (false?
        (-> (partial/get-element incorrect-lang) (get :body) (clojure.string/includes? "table"))))
  (is (false? (-> (partial/get-element incorrect-col) (get :body) (clojure.string/includes? "table"))))
  (is (false? (-> (partial/get-element incorrect-ord) (get :body) (clojure.string/includes? "table"))))
  (is (false? (-> (partial/get-element incorrect-cid) (get :body) (clojure.string/includes? "table")))))
