(ns techdoc.dashboard.cache-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.web.controllers.cache :as cache]))


(set! *warn-on-reflection* true)


(deftest cache
  (is (nil? (cache/just-fetch-record "miss"))) ; return nil for cache miss
  (is (true? (cache/fetch-cache-record "found" (constantly true)))) ; fetch and cache record
  (is (true? (cache/just-fetch-record "found")))) ; fetch record cached in previous assertion
