(ns techdoc.dashboard.core-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.test-utils :as utils]))


(set! *warn-on-reflection* true)


(deftest example-test
  (is (= 1 1)))
