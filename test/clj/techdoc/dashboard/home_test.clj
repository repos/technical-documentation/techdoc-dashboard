(ns techdoc.dashboard.home-test
  (:require
    [clojure.string :as str]
    [clojure.test :refer :all]
    [hiccup.page :as p]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.ui.home :as home]))


(set! *warn-on-reflection* true)


(def pagepile-4999
  (pagepile/get-pagepile-structured "4999" :en false))


(def pagepile-54482
  (pagepile/get-pagepile-structured "54482" :en false))


(deftest button
  (is (.equals "test"
               (last (home/button {:label "test"}))))
  (is (= :button.btn
         (first (home/button {}))))
  (is (.equals "Submit"
               (last (home/button {})))))


(deftest home-page-ui
  (is (= true
         (str/includes? (p/html5 (home/home-page-ui :en "test")) "test")
         (str/includes? (p/html5 (home/home-page-ui :en "test")) "Specify a PagePile ID")
         (str/includes? (p/html5 (home/home-page-ui :pl "test")) "Podaj identyfikator"))))


(deftest home-page
  (is (true? (str/includes? (home/home-page {:params {:lang "en"}}) "Specify"))))


(deftest home-page-no-js
  (is (true?
        (str/includes?
          (home/home-page-no-js {:params {:lang "en", :pagepile "47924"}})
          "Release_Engineering"))))


(deftest load-pile
  (is (true?
        (str/includes?
          (home/load-pile {:params {:lang "en", :pagepile "47924"}})
          "Release_Engineering"))))


(deftest format-pagepile-contents
  (is (.equals "Invalid PagePile ID"
               (last (flatten (home/format-pagepile-contents pagepile-4999)))))
  (is (= :ul
         (first (flatten (home/format-pagepile-contents pagepile-54482))))))
