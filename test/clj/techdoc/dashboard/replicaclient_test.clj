(ns techdoc.dashboard.replicaclient-test
  (:require
    [clojure.test :refer :all]
    [techdoc.dashboard.lib.common :as common]
    [techdoc.dashboard.lib.replica-client :as replica]))


(set! *warn-on-reflection* true)


(def pagepile-data
  {:pages ["Toolforge/Pywikibot" "Portal:Cloud_VPS"]
   :wiki "wikitechwikimedia"
   :start-date "20231017"
   :end-date "20240115"})


(def revisionsizes-subquery-sql
  (replica/gen-revision-sizes-subquery
    (:pages pagepile-data)
    (:wiki pagepile-data)
    (:start-date pagepile-data)
    (:end-date pagepile-data)))


(def revisionsizes-sql
  (replica/gen-revision-sizes-sql
    (:pages pagepile-data)
    (:wiki pagepile-data)
    (:start-date pagepile-data)
    (:end-date pagepile-data)))


(def topeditors-sql
  (replica/gen-editors-sql
    (:pages pagepile-data)
    (:wiki pagepile-data)
    (:start-date pagepile-data)
    (:end-date pagepile-data)))


(def revisions-sql
  (replica/gen-revisions-sql
    (:pages pagepile-data)
    (:wiki pagepile-data)
    (:start-date pagepile-data)
    (:end-date pagepile-data)))


(def templates-sql
  (replica/gen-templates-sql
    (:pages pagepile-data)
    (:wiki pagepile-data)))


(def rs-subquery
  {:select
   [[[:if
      [:>=
       [:abs
        [:- [:cast :r.rev_len :signed] [:cast [:ifnull :pr.rev_len 0] :signed]]]
       1000]
      true
      false]
     :big_edit]
    [:r.rev_minor_edit :minor_edit]],
   :from [[:page :p]],
   :join-by
   [:left
    [[:revision :r] [:= :r.rev_page :p.page_id]]
    :left
    [[:revision :pr] [:= :pr.rev_id :r.rev_parent_id]]],
   :where
   [:and
    [:= :r.rev_deleted 0]
    [:between [:date :r.rev_timestamp] "20231017" "20240115"]
    [:or
     [:and
      [:in :p.page_title ["Toolforge/Pywikibot" "Portal:Cloud_VPS"]]
      [:= :p.page_namespace 0]]
     nil]]})


(def rs-fullquery
  ["\nSELECT q.big_edit AS big_edit, q.minor_edit AS minor_edit, COUNT(*) AS number_of_edits\nFROM (SELECT IF(ABS(CAST(r.rev_len AS SIGNED) - CAST(IFNULL(pr.rev_len, ?) AS SIGNED)) >= ?, TRUE, FALSE) AS big_edit, r.rev_minor_edit AS minor_edit FROM page AS p LEFT JOIN revision AS r ON r.rev_page = p.page_id LEFT JOIN revision AS pr ON pr.rev_id = r.rev_parent_id WHERE (r.rev_deleted = ?) AND DATE(r.rev_timestamp) BETWEEN ? AND ? AND (((p.page_title IN (?, ?)) AND (p.page_namespace = ?)))) AS q\nGROUP BY big_edit, minor_edit\n"
   0
   1000
   0
   "20231017"
   "20240115"
   "Toolforge/Pywikibot"
   "Portal:Cloud_VPS"
   0])


(def te-fullquery
  ["\nSELECT a.actor_name AS name, COUNT(r.rev_id) AS edits\nFROM page AS p\nLEFT JOIN revision AS r ON r.rev_page = p.page_id LEFT JOIN actor AS a ON r.rev_actor = a.actor_id\nWHERE (r.rev_deleted = ?) AND DATE(r.rev_timestamp) BETWEEN ? AND ? AND (((p.page_title IN (?, ?)) AND (p.page_namespace = ?)))\nGROUP BY name\nORDER BY edits DESC\nLIMIT ?\n"
   0
   "20231017"
   "20240115"
   "Toolforge/Pywikibot"
   "Portal:Cloud_VPS"
   0
   10])


(def r-fullquery
  ["\nSELECT DATE(LEFT(r.rev_timestamp, 8)) AS d, COUNT(r.rev_id) AS c\nFROM page AS p\nLEFT JOIN revision AS r ON r.rev_page = p.page_id LEFT JOIN actor AS a ON r.rev_actor = a.actor_id\nWHERE (r.rev_deleted = ?) AND DATE(r.rev_timestamp) BETWEEN ? AND ? AND (((p.page_title IN (?, ?)) AND (p.page_namespace = ?)))\nGROUP BY d\nORDER BY r.rev_timestamp ASC\n"
   0
   "20231017"
   "20240115"
   "Toolforge/Pywikibot"
   "Portal:Cloud_VPS"
   0])


(def t-fullquery
  ["\nSELECT p.page_title, lt.lt_title\nFROM templatelinks AS tl\nINNER JOIN page AS p ON tl.tl_from = p.page_id INNER JOIN linktarget AS lt ON tl.tl_target_id = lt.lt_id\nWHERE (lt.lt_namespace = ?) AND (((p.page_title IN (?, ?)) AND (p.page_namespace = ?))) AND (lt.lt_title IN (?, ?, ?, ?, ?, ?, ?, ?))\nORDER BY lt.lt_title ASC, p.page_title ASC\n"
   10
   "Toolforge/Pywikibot"
   "Portal:Cloud_VPS"
   0
   "Archive"
   "Draft"
   "Draft-section"
   "Fixme"
   "Fixme_inline"
   "Historical"
   "Outdated"
   "Outdated-inline"])


(deftest process-page-test
  (is (= [:unchanged-struct]
         (replica/process-page [:unchanged-struct] "Incorrect:Page" "wikitechwikimedia")))
  (is (= [[:and [:= :p.page_title "Page"] [:= :p.page_namespace 4]]]
         (replica/process-page [] "Project:Page" "wikitechwikimedia"))))


(deftest getds-test
  (is (not= nil
            (replica/get-ds "wikitechwikimedia")))
  (is (not= nil
            (replica/get-ds "mediawikiwiki")))
  (is (not= nil
            (replica/get-ds "metawiki"))))


(def genwhere-example
  [:or [:and [:= :p.page_title "Page"] [:= :p.page_namespace 4]]])


(def genwhere-complex-example
  [:or
   [:and [:= :p.page_title "Page"] [:= :p.page_namespace 4]]
   [:and [:= :p.page_title "Page2"] [:= :p.page_namespace 4]]])


(deftest genwhere-test
  (is (= genwhere-example
         (replica/gen-where ["Project:Page"] "wikitechwikimedia")))
  (is (= genwhere-complex-example
         (replica/gen-where ["Project:Page" "Project:Page2"] "wikitechwikimedia")))
  (is (= [:or]
         (replica/gen-where ["Test-page"] "wikitechwikimedia"))))


(deftest sql-queries
  (is (= rs-subquery
         revisionsizes-subquery-sql))
  (is (= rs-fullquery
         revisionsizes-sql))
  (is (= te-fullquery
         topeditors-sql))
  (is (= r-fullquery
         revisions-sql))
  (is (= t-fullquery
         templates-sql)))


(def rows
  [{:big_edit 0, :revision/minor_edit 0, :number_of_edits 1}
   {:big_edit 0, :revision/minor_edit 1, :number_of_edits 2}
   {:big_edit 1, :revision/minor_edit 0, :number_of_edits 3}
   {:big_edit 1, :revision/minor_edit 1, :number_of_edits 4}])


(deftest normalization
  (is (= (replica/normalize-revisionsizes rows)
         {:big_edits 7
          :small_edits 3
          :minor_edits 6
          :major_edits 4}))
  (is (= (common/dates-structure)
         (replica/normalize-revisions [])))
  (is (= (conj (common/dates-structure) {:2024010100 1})
         (replica/normalize-revisions [{:c 1 :d "2024010100"}]))))
