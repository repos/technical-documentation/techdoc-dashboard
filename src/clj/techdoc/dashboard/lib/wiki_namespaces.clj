(ns techdoc.dashboard.lib.wiki-namespaces
  "Gives you access to namespaces used on permitted-wikis.
  Loads the namespaces when the application is started."
  (:require
    [clj-http.client :as client]
    [clojure.tools.logging :as log]
    [techdoc.dashboard.config :as config]))


(set! *warn-on-reflection* true)


(defn fetch-namespaces
  "Requests and directly returns a map containing namespaces
  available on the specified `wiki`. Use `format-namespaces-response`
  to prepare this data for usage in SQL query generation."
  [wiki]
  (try (client/get (get-in config/permitted-wikis [wiki :api])
                   {:headers {"User-Agent" config/user-agent}
                    :as :json
                    :query-params {"action" "query"
                                   "meta" "siteinfo"
                                   "siprop" "namespaces"
                                   "format" "json"
                                   "formatversion" 2}})
       (catch Exception e
         (log/error e))))


(defn format-namespaces-response
  "Reformats the response returned from querying wiki APIs about available
  namespaces into a structure more easily applied for generating SQL queries.

  Example of the returned data structure:
  {\"Manual\" 100
   nil 0}

  Takes the unedited response `r` as the only parameter"
  [r]
  (let [namespaces (get-in r [:body :query :namespaces])]
    (reduce-kv #(assoc %1 (:canonical %3) (read-string (subs (str %2) 1))) {} namespaces)))


(defn get-wiki-namespaces
  "Makes a request to the API of `wiki` to download the list
  of namespaces used on that wiki. This function does not include
  throttling and should only execute once per wiki during
  application startup.
  "
  [wiki]
  (-> wiki
      fetch-namespaces
      format-namespaces-response))


(def ^{:doc "Map with lists of namespaces available on supported wikis. This structure is filled with data once, during startup."}
  wiki-namespaces
  (when-not *compile-files*
    (let [wikitech-namespaces (get-wiki-namespaces "wikitechwikitechwikimedia")]
      {"mediawikiwiki" (get-wiki-namespaces "mediawikiwiki")
       "metawiki" (get-wiki-namespaces "metawiki")
       "wikitechwikitechwikimedia" wikitech-namespaces
       "wikitechwikimedia" wikitech-namespaces})))
