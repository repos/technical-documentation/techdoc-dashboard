(ns techdoc.dashboard.lib.replica-client
  "Functions for interacting with wiki replicas."
  (:require
    [clojure.string :as str]
    [clojure.tools.logging :as log]
    [honey.sql :as sql]
    [java-time.api :as jt]
    [next.jdbc :as jdbc]
    [next.jdbc.result-set :as rs]
    [next.jdbc.sql :as jdbc-sql]
    [techdoc.dashboard.config :as config]
    [techdoc.dashboard.lib.common :as common]
    [techdoc.dashboard.lib.wiki-namespaces :as wn]
    [techdoc.dashboard.web.controllers.cache :refer [fetch-cache-record]]
    [throttler.core :refer  [fn-throttler]]))


(set! *warn-on-reflection* true)


;; set dialect for all SQL queries
;; there is no separate dialect for mariadb
(sql/set-dialect! :mysql)


;; formatting setting for data returned through SQL queries
;; SQL dates are formatted as yyyyMMdd00
(extend-protocol rs/ReadableColumn
  java.sql.Date
  (read-column-by-label [^java.sql.Date v _]
    (jt/format "yyyyMMdd00" (jt/local-date v)))
  (read-column-by-index [^java.sql.Date v _2 _3]
    (jt/format "yyyyMMdd00" (jt/local-date v))))


;; conversion settings for data stored as varbinary
;; convert varbinary to string
(extend-protocol rs/ReadableColumn
  (Class/forName "[B")
  (read-column-by-label [^"[B" v _]
    (String. ^"[B" v))
  (read-column-by-index [^"[B" v _2 _3]
    (String. ^"[B" v)))


(def ^{:doc "Rate-limiting configuration for replica requests: 1/s."}
  replica-throttler
  (fn-throttler 1 :second))


(defn get-ds
  "Generates a datasource for use in database calls. This
  datasource is based on the database configuration structure
  included in the specification of a permitted wiki."
  [wiki]
  (let [db (get-in config/permitted-wikis [wiki :db])]
    (if (nil? db)
      (throw (Exception. (str "Cannot create datasource for wiki: " wiki ", unsupported wiki")))
      (jdbc/get-datasource db))))


(defn process-page
  "Generates a `where` constraint to match a page. If the title of a page `p`
  contains `:`, it might consist of page title and namespace. This function checks
  if the potential namespace is in the list of namespaces for the given wiki `w`. If
  that's the case, the combination of page namespace and title is added to the `where`
  statement (represented by the `s` data structure). Otherwise, the function returns
  an unchanged structure `s`."
  [s p w]
  (let [r (str/split p #":" 2)
        l (count r)]
    (if (and (= l 2) (some? (get-in wn/wiki-namespaces [w (nth r 0)])))
      (conj s [:and
               [:= :p.page_title (nth r 1)]
               [:= :p.page_namespace (get-in wn/wiki-namespaces [w (nth r 0)])]])
      s)))


(defn gen-where
  "Generates the `where` part of the SQL query using
  [[process-page]]. This is a wrapper that puts the
  result of that function inside an appropriate
  [HoneySQL](https://github.com/seancorfield/honeysql)
  structure."
  [l w]
  (reduce (fn [s p] (process-page s p w)) [:or] l))


(defn generate-constraints
  "Generates two types of constraints for the SQL query:

  * `in` - to identify pages that match titles from the page list exactly
  * `where` - to identify page and namespace combinations that match titles from the page list"
  [l w]
  (let [gw (gen-where l w)
        lgw (count gw)]
    (if (= 1 lgw)
      {:in l}
      {:in l
       :where gw})))


(defn gen-revisions-sql
  "Generates the SQL expression to execute to
  fetch revision information from the database. Specified
  using [HoneySQL syntax](https://github.com/seancorfield/honeysql).

  Takes the following arguments:

  * `l` - list of pages
  * `w` - wiki
  * `start-date` - beginning of revision date range
  * `end-date` - end of revision date range"
  [l w start-date end-date]
  (let [constraints (generate-constraints l w)
        in-constraint (:in constraints)
        where-constraint (:where constraints)]
    (sql/format {:select [[[:date [:left :r.rev_timestamp :8]] :d]
                          [[:count :r.rev_id] :c]]
                 :from [[:page :p]]
                 :join-by [:left [[:revision :r]
                                  [:= :r.rev_page :p.page_id]]
                           :left [[:actor :a]
                                  [:= :r.rev_actor :a.actor_id]]]
                 :where [:and
                         [:= :r.rev_deleted 0]
                         [:between [:date :r.rev_timestamp] start-date end-date]
                         [:or
                          [:and [:in :p.page_title in-constraint] [:= :p.page_namespace 0]]
                          where-constraint]]
                 :group-by [:d]
                 :order-by [:r.rev_timestamp]}
                {:pretty true})))


;; SELECT DATE(LEFT(r.rev_timestamp, 8)) AS d, COUNT(r.rev_id) AS c
;; FROM page AS p
;; LEFT JOIN revision AS r ON r.rev_page = p.page_id LEFT JOIN actor AS a ON r.rev_actor = a.actor_id
;; WHERE (r.rev_deleted = 0) AND DATE(r.rev_timestamp) BETWEEN "20230330" AND "20230930" AND (((p.page_title IN ("Page Title")) AND (p.page_namespace = 0)))
;; GROUP BY d
;; ORDER BY r.rev_timestamp ASC

(defn gen-editors-sql
  "Generates the SQL expression to execute to
  fetch editor information from the database. Specified
  using [HoneySQL syntax](https://github.com/seancorfield/honeysql).

  Takes the following arguments:

  * `l` - list of pages
  * `w` - wiki
  * `start-date` - beginning of revision date range
  * `end-date` - end of revision date range"
  [l w start-date end-date]
  (let [constraints (generate-constraints l w)]
    (sql/format {:select [[:a.actor_name :name]
                          [[:count :r.rev_id] :edits]]
                 :from [[:page :p]]
                 :join-by [:left [[:revision :r]
                                  [:= :r.rev_page :p.page_id]]
                           :left [[:actor :a]
                                  [:= :r.rev_actor :a.actor_id]]]
                 :where [:and
                         [:= :r.rev_deleted 0]
                         [:between [:date :r.rev_timestamp] start-date end-date]
                         [:or
                          [:and [:in :p.page_title (:in constraints)] [:= :p.page_namespace 0]]
                          (:where constraints)]]
                 :group-by :name
                 :order-by [[:edits :desc]]
                 :limit 10}
                {:pretty true})))


;; SELECT a.actor_name AS name, COUNT(r.rev_id) AS edits
;; FROM page AS p
;; LEFT JOIN revision AS r ON r.rev_page = p.page_id LEFT JOIN actor AS a ON r.rev_actor = a.actor_id
;; WHERE (r.rev_deleted = 0) AND DATE(r.rev_timestamp) BETWEEN "20230330" AND "20230930" AND (((p.page_title IN ("Page Title")) AND (p.page_namespace = 0)))
;; GROUP BY name
;; ORDER BY edits DESC
;; LIMIT 10


(defn gen-revision-sizes-subquery
  "Generates the SQL sub-query to execute to
  fetch revision size information from the database. Specified
  using [HoneySQL syntax](https://github.com/seancorfield/honeysql).

  Takes the following arguments:

  * `l` - list of pages
  * `w` - wiki
  * `start-date` - beginning of revision date range
  * `end-date` - end of revision date range"
  [l w start-date end-date]
  (let [constraints (generate-constraints l w)]
    {:select [[[:if [:>= [:abs [:-
                                [:cast :r.rev_len :signed]
                                [:cast [:ifnull :pr.rev_len 0] :signed]]]
                     1000]
                true
                false]
               :big_edit]
              [:r.rev_minor_edit :minor_edit]]
     :from [[:page :p]]
     :join-by [:left [[:revision :r]
                      [:= :r.rev_page :p.page_id]]
               :left [[:revision :pr]
                      [:= :pr.rev_id :r.rev_parent_id]]]
     :where [:and
             [:= :r.rev_deleted 0]
             [:between [:date :r.rev_timestamp] start-date end-date]
             [:or
              [:and [:in :p.page_title (:in constraints)] [:= :p.page_namespace 0]]
              (:where constraints)]]}))


(defn gen-revision-sizes-sql
  "Generates the outer SQL query to execute to
  fetch revision size information from the database based on the sub-query
  generated in [[gen-revision-sizes-subquery]]. Specified using
  [HoneySQL syntax](https://github.com/seancorfield/honeysql).

  Takes the following arguments:

  * `l` - list of pages
  * `w` - wiki
  * `start-date` - beginning of revision date range
  * `end-date` - end of revision date range"
  [l w start-date end-date]
  (sql/format {:select [[:q.big_edit :big_edit]
                        [:q.minor_edit :minor_edit]
                        [[:count :*] :number_of_edits]]
               :from [[(gen-revision-sizes-subquery l w start-date end-date) :q]]
               :group-by [:big_edit :minor_edit]}
              {:pretty true}))


;; SELECT q.big_edit AS big_edit, q.minor_edit AS minor_edit, COUNT(*) AS number_of_edits
;; FROM (
;;   SELECT IF(ABS(CAST(r.rev_len AS SIGNED) - CAST(IFNULL(pr.rev_len, 0) AS SIGNED)) >= 1000, TRUE, FALSE) AS big_edit, r.rev_minor_edit AS minor_edit
;;   FROM page AS p
;;   LEFT JOIN revision AS r ON r.rev_page = p.page_id
;;   LEFT JOIN revision AS pr ON pr.rev_id = r.rev_parent_id
;;   WHERE (r.rev_deleted = 0) AND DATE(r.rev_timestamp) BETWEEN "20230330" AND "20230930" AND (((p.page_title IN ("Page Title")) AND (p.page_namespace = 0)))) AS q
;; GROUP BY big_edit, minor_edit

(defn gen-templates-sql
  "Generates a query used to load information about template
  usage on pages in the pile.

  Takes the following arguments:

  * `l` - list of pages
  * `w` - wiki"
  [l w]
  (let [constraints (generate-constraints l w)
        templates (get-in config/permitted-wikis [w :templates])]
    (sql/format {:select [[:p.page_title]
                          [:lt.lt_title]]
                 :from [[:templatelinks :tl]]
                 :join-by [:inner [[:page :p]
                                   [:= :tl.tl_from :p.page_id]]
                           :inner [[:linktarget :lt]
                                   [:= :tl.tl_target_id :lt.lt_id]]]
                 :where [:and
                         [:= :lt.lt_namespace 10]
                         [:or
                          [:and [:in :p.page_title (:in constraints)] [:= :p.page_namespace 0]]
                          (:where constraints)]
                         [:in :lt.lt_title templates]]
                 :order-by [[:lt.lt_title :asc]
                            [:p.page_title :asc]]}
                {:pretty true})))


;; SELECT p.page_title, lt.lt_title
;; FROM templatelinks AS tl
;; INNER JOIN page AS p ON tl.tl_from = p.page_id INNER JOIN linktarget AS lt ON tl.tl_target_id = lt.lt_id
;; WHERE (lt.lt_namespace = 10)
;; AND (((p.page_title IN ("Page Title")) AND (p.page_namespace = 0)))
;; AND (lt.lt_title IN ("Draft", "Draft-section", "Historical", "DoNotTranslate", "Outdated2"))
;; ORDER BY lt.lt_title ASC, p.page_title ASC

(defn run-sql-query
  "Generic SQL query runner used in multiple functions"
  [ds query]
  (try (jdbc-sql/query ds query)
       (catch Exception e
         (log/error "Error: " (.getMessage e))
         [])))


(defn get-revisions
  "Runs the SQL query generated using [[gen-revisions-sql]]
  on the datasource generated using [[get-ds]]."
  [pages wiki date-from date-to]
  (run-sql-query (get-ds wiki) (gen-revisions-sql pages wiki date-from date-to)))


(defn get-editors
  "Runs the SQL query generated using [[gen-editors-sql]]
  on the datasource generated using [[get-ds]]"
  [list-of-pages wiki start-date end-date]
  (run-sql-query (get-ds wiki) (gen-editors-sql list-of-pages wiki start-date end-date)))


(defn get-revision-sizes
  "Runs the SQL query generated using [[gen-revision-sizes-sql]]
  on the datasource generated using [[get-ds]]"
  [list-of-pages wiki start-date end-date]
  (run-sql-query (get-ds wiki) (gen-revision-sizes-sql list-of-pages wiki start-date end-date)))


(defn get-templates
  "Runs the SQL query generated using [[gen-templates-sql]]
  on the datasource generated using [[get-ds]]"
  [list-of-pages wiki]
  (run-sql-query (get-ds wiki) (gen-templates-sql list-of-pages wiki)))


(def ^{:doc "Throttled (limited to one execution per second) version of the
       [[get-revisions]] function."}
  gr-slow
  (replica-throttler get-revisions))


(def ^{:doc "Throttled (limited to one execution per second) version of the
       [[get-editors]] function."}
  ge-slow
  (replica-throttler get-editors))


(def ^{:doc "Throttled (limited to one execution per second) version of the
       [[get-revision-sizes]] function."}
  grs-slow
  (replica-throttler get-revision-sizes))


(def ^{:doc "Throttled (limited to one execution per second) version of the
       [[get-templates]] function."}
  gt-slow
  (replica-throttler get-templates))


(defn- fetch-revisions-basedon-key
  "Loads revision data based on key and the list of pages. The `key`
  should have the following format:
  \"revisions/<wiki>~~|<pagepile-id>~~|<start-date>~~|<end-date>\"

  `pages` is a vector with page names."
  [key pages]
  (let [id (str/split (name key) #"~~\|")
        wiki (nth id 0)
        pagepile-id (nth id 1) ; not used, kept for completeness
        date-90 (nth id 2)
        date-now (nth id 3)]
    (gr-slow pages wiki date-90 date-now)))


(defn- fetch-editors-basedon-key
  "Loads most active editor data based on key and the list of pages.
  The `key` should have the following format:
  \"editors/<wiki>~~|<pagepile-id>~~|<start-date>~~|<end-date>\"

  `pages` is a vector with page names."
  [key pages]
  (let [id (str/split (name key) #"~~\|")
        wiki (nth id 0)
        pagepile-id (nth id 1)
        date-90 (nth id 2)
        date-now (nth id 3)]
    (ge-slow pages wiki date-90 date-now)))


(defn- fetch-revisionsizes-basedon-key
  "Loads revision size data based on key and the list of pages.
  The `key` should have the following format:
  \"revisionsizes/<wiki>~~|<pagepile-id>~~|<start-date>~~|<end-date>\"

  `pages` is a vector with page names."
  [key pages]
  (let [id (str/split (name key) #"~~\|")
        wiki (nth id 0)
        pagepile-id (nth id 1)
        date-90 (nth id 2)
        date-now (nth id 3)]
    (grs-slow pages wiki date-90 date-now)))


(defn- fetch-templates-basedon-key
  "Loads template data based on key and the list of pages.
  The `key` should have the following format:
  \"templates/<wiki>~~|<pagepile-id>\"

  `pages` is a vector with page names."
  [key pages]
  (let [id (str/split (name key) #"~~\|")
        wiki (nth id 0)
        pagepile-id (nth id 1)]
    (gt-slow pages wiki)))


(defn get-revisions-through-cache
  "Loads revisions for `pagepile-id`, `pages`, and `wiki` through cache.
  If the cache doesn't contain information for these arguments, executes
  [[fetch-revisions-basedon-key]] with the generated key and the list
  of pages passed as arguments, then caches the results."
  [pagepile-id pages wiki]
  (fetch-cache-record (keyword "revisions" (str wiki "~~|" pagepile-id "~~|" (common/UTC-date-90) "~~|" (common/UTC-date-yesterday))) #(fetch-revisions-basedon-key % pages)))


(defn get-editors-through-cache
  "Loads most active editors for `pagepile-id`, `pages`, and `wiki` through cache.
  If the cache doesn't contain information for these arguments, executes
  [[fetch-editors-basedon-key]] with the generated key and the list of pages
  passed as arguments, then caches the results."
  [pagepile-id pages wiki]
  (fetch-cache-record (keyword "editors" (str wiki "~~|" pagepile-id "~~|" (common/UTC-date-90) "~~|" (common/UTC-date-yesterday))) #(fetch-editors-basedon-key % pages)))


(defn get-revisionsizes-through-cache
  "Loads revision size data for `pagepile-id`, `pages`, and `wiki` through cache.
  If the cache doesn't contain information for these arguments, executes
  [[fetch-revisionsizes-basedon-key]] with the generated key and the list of pages
  passed as arguments, then caches the results."
  [pagepile-id pages wiki]
  (fetch-cache-record (keyword "revisionsizes" (str wiki "~~|" pagepile-id "~~|" (common/UTC-date-90) "~~|" (common/UTC-date-yesterday))) #(fetch-revisionsizes-basedon-key % pages)))


(defn get-templates-through-cache
  "Loads template data for `pagepile-id`, `pages`, and `wiki` through cache.
  If the cache doesn't contain information for these arguments, executes
  [[fetch-templates-basedon-key]] with the generated key and the list of pages
  passed as arguments, then caches the results."
  [pagepile-id pages wiki]
  (fetch-cache-record (keyword "templates" (str wiki "~~|" pagepile-id)) #(fetch-templates-basedon-key % pages)))


(defn normalize-revisions
  "Normalizes revision data returned by the database. Returns a map
  with 90 key-value pairs, where each key represents a date in the last
  90 days, and each value - a revision count for that date.

  Keys are formatted according to date formatting settings in
  [[common/dates-structure]]. Values are integers.

  Expects `revisions` to be a vector, where each day is represented by a
  map in the following format: `{:d \"yyyyMMdd00\" :c 0}`.

  Example: `(normalize [{:c 1 :d \"2023010100\"}])`."
  [revisions]
  (reduce #(conj %1 {(keyword (:d %2)) (:c %2)}) (common/dates-structure) revisions))


(defn get-revisions-90d
  "Returns revisions data for the last 90 days. Takes the following arguments:

  * `pagepile-id` - used in cache key
  * `pages` - list of pages to get revisions for
  * `wiki` - name of the wiki which hosts the pages

  You can call this function directly in the UI, for example in
  [[techdoc.dashboard.web.ui.dashboard/dashboard]]. It returns a
  data structure that needs to be wrapped in Hiccup code, for
  example like in [[techdoc.dashboard.web.ui.dashboard/pageedits-widget]]."
  [pagepile-id pages wiki]
  (let [revisions (get-revisions-through-cache pagepile-id pages wiki)]
    (->> revisions
         (normalize-revisions)
         seq
         (map (fn [r] {:date (name (first r)) :value (last r)}))
         (sort-by :date)
         (into []))))


(defn get-editors-90d
  "Returns editor data for the last 90 days. Takes the following arguments:

  * `pagepile-id` - used in cache key
  * `pages` - list of pages to get editor information for
  * `wiki` - name of the wiki which hosts the pages

  You can call this function directly in the UI, for example in
  [[techdoc.dashboard.web.ui.dashboard/dashboard]]. It returns a
  data structure that needs to be wrapped in Hiccup code, for
  example like in [[techdoc.dashboard.web.ui.dashboard/topeditors-widget]]."
  [pagepile-id pages wiki]
  (->> (get-editors-through-cache pagepile-id pages wiki)
       (map (fn [r] {:editor (:actor/name r) :edits (:edits r)}))))


(defn normalize-revisionsizes
  "Normalizes revision size data. Converts a vector of maps passed
  in `t` - [{:big_edit 1 :minor_edit 0 :number_of_edits <number>}
            {:big_edit 0 :minor_edit 0 :number_of_edits <number>}
            ... ] -
  to a single map: {:big_edits <number> :small_edits <number>
                    :major_edits <number> :minor_edits <number>}."
  [t]
  (reduce (fn [output row]
            (let [big_edit (:big_edit row)
                  minor_edit (:revision/minor_edit row)
                  number_of_edits (:number_of_edits row)
                  edit_size (if (= big_edit 1) :big_edits :small_edits)
                  edit_importance (if (= minor_edit 1) :minor_edits :major_edits)]
              (-> output
                  (assoc edit_size (+ (get output edit_size) number_of_edits))
                  (assoc edit_importance (+ (get output edit_importance) number_of_edits)))))
          {:big_edits 0 :small_edits 0 :minor_edits 0 :major_edits 0}
          t))


(defn get-revisionsizes-90d
  "Returns revision size data for the last 90 days. Takes the following arguments:

  * `pagepile-id` - used in cache key
  * `pages` - list of pages to get editor information for
  * `wiki` - name of the wiki which hosts the pages

  You can call this function directly in the UI, for example in
  [[techdoc.dashboard.web.ui.dashboard/dashboard]]. It returns a
  data structure that needs to be wrapped in Hiccup code."
  [pagepile-id pages wiki]
  (-> (get-revisionsizes-through-cache pagepile-id pages wiki)
      (normalize-revisionsizes)))


(defn get-templates-current
  "Returns current template data. Takes the following arguments:

  * `pagepile-id` - used in cache key
  * `pages` - list of pages to get template information for
  * `wiki` - name of the wiki which hosts the pages

  You can call this function directly in the UI, for example in
  [[techdoc.dashboard.web.ui.dashboard/dashboard]]. It returns a
  data structure that needs to be wrapped in Hiccup code."
  [pagepile-id pages wiki]
  (->> (get-templates-through-cache pagepile-id pages wiki)
       (map (fn [r] {:page (:page/page_title r) :template (:linktarget/lt_title r)}))))
