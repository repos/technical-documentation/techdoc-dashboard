(ns techdoc.dashboard.lib.common
  "Common functions used in other namespaces."
  (:require
    [java-time.api :as jt]))


(set! *warn-on-reflection* true)


(defn dates-structure
  "Generates a map with a zero for every day in the last 90 days. Intended to be
  used with [[calculate-sum]]. This ensures that the pageviews data structure
  always has records for all 90 days. It is necessary because the API does not
  return data for days with zero pageviews, making it difficult to draw the chart correctly.
  "
  []
  (let [now (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 1))]
    (->> (take 90 (jt/iterate jt/minus now (jt/days 1)))
         (reduce #(conj %1 {(keyword (jt/format "yyyyMMdd00" %2)) 0}) {}))))


(defn calculate-sum
  "Merges a collection of maps (for example, containing pageview values for different pages)
  into a single map, adding values under the same key.

  Addition works like below:

  `[{:a 10 :b 20} {:a 1 :b 2}]` becomes:
  `{:a 11 :b 22}`
  "
  [collection]
  (apply merge-with + collection))


(defn UTC-date-now
  "Returns current UTC date"
  []
  (jt/format "yyyyMMdd" (jt/with-zone (jt/zoned-date-time) "UTC")))


(defn UTC-date-yesterday
  "Returns UTC date for yesterday"
  []
  (jt/format "yyyyMMdd" (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 1))))


(defn UTC-date-90
  "Returns UTC date of the day 90 days ago"
  []
  (jt/format "yyyyMMdd" (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 90))))
