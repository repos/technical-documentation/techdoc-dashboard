(ns techdoc.dashboard.lib.mw-client
  "Functions for interacting with MediaWiki APIs."
  (:require
    [clj-http.client :as client]
    [clojure.string :as str]
    [clojure.tools.logging :as log]
    [ring.util.codec :refer [url-encode]]
    [techdoc.dashboard.config :as config]
    [techdoc.dashboard.lib.common :as common]
    [techdoc.dashboard.web.controllers.cache :refer [fetch-cache-record just-fetch-record]]
    [throttler.core :refer  [fn-throttler]]))


(set! *warn-on-reflection* true)


(def ^{:doc "Creates a rate-limiting configuration for Action API requests: 1/s.
             Use it when defining a rate-limited version of a fetch function."}
  api-throttler
  (fn-throttler 1 :second))


(def ^{:doc "Creates a rate-limiting configuration for REST API requests: 90/s.
             Use it when defining a rate-limited version of a fetch function."}
  rest-throttler
  (fn-throttler 90 :second))


(defn fetch-general-page-data
  "Fetches information about outgoing and incoming links,
  general page info, categories, and templates used on a `page`
  on a given `wiki`. For a list of supported wikis, see [[config/permitted-wikis]]

  Example:
  ```(fetch-general-page-data \"Help:Toolforge\" \"wikitechwikitechwikimedia\")```"
  [page wiki]
  (let [wiki-url (get-in config/permitted-wikis [wiki :api])]
    (client/get wiki-url
                {:headers {"User-Agent" config/user-agent}
                 :as :json
                 :query-params {"action" "query"
                                "format" "json"
                                "prop" "linkshere|links|info|categories|templates"
                                "titles" page
                                "lhlimit" "max"
                                "cllimit" "max"
                                "pllimit" "max"}})))


(defn fetch-page-preview
  "Fetches content of a `page` from a given `wiki`.
  For a list of supported wikis, see [[config/permitted-wikis.]]

  Example:
  ```(fetch-page-preview \"Help:Toolforge\" \"wikitechwikitechwikimedia\")```"
  [page wiki]
  (client/get (get-in config/permitted-wikis [wiki :api])
              {:headers {"User-Agent" config/user-agent}
               :as :json
               :query-params {"action" "parse"
                              "format" "json"
                              "page" page}}))


(defn- fetch-page-views-data
  "Fetches pageview data based on the required parameters. Only use this function
  for testing and troubleshooting purposes. In other circumstances, use the
  rate-limited and cached functions listed below."
  [page wiki d-90 d-now]
  (log/info "Fetching page view data for: " page)
  (->
    (try (client/get (str "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/"
                          (get-in config/permitted-wikis [wiki :id])
                          "/all-access/all-agents/"
                          (url-encode page)
                          "/daily/"
                          d-90
                          "/"
                          d-now)
                     {:as :json
                      :headers {"User-Agent" config/user-agent}
                      :throw-entire-message? true})
         (catch Exception e
           (log/error "Error: " (.getMessage e))
           {:body {:items []}}))
    (get-in [:body :items])))


(defn- fetch-page-data
  "Fetches page wikitext content for a page or group of pages
  in the `pages` parameter. "
  [pages wiki]
  (log/info (str "Fetching page data for: " pages))
  (try
    (client/get (get-in config/permitted-wikis [wiki :api])
                {:headers {"User-Agent" config/user-agent}
                 :as :json
                 :query-params {"action" "query"
                                "format" "json"
                                "prop" "revisions"
                                "titles" pages
                                "formatversion" "2"
                                "rvprop" "content"
                                "rvslots" "*"}})
    (catch Exception e
      (log/error "Error: " (.getMessage e))
      {:body {:query {:pages []}}})))


(defn normalize-translate-data
  "Checks data returned by [[fetch-page-data]] for translate syntax.
  Turns the data returned by the API into a simple vector
  where each page has one record in the following format:
  `{:page \"Page title\" :translate <true|false|nil>}`"
  [translate-data]
  (let [td (get-in translate-data [:body :query :pages])]
    (map (fn [page]
           (let [title (:title page)
                 content (get-in (first (:revisions page)) [:slots :main :content])]
             {:page  title
              :translate (when (some? content) (clojure.string/includes? content "<translate>"))})) td)))


;; Rate-limited versions of fetch functions implemented above

(def ^{:doc "Rate-limited version of [[fetch-page-preview]]"}
  fgp-slow
  (api-throttler fetch-page-preview))


(def ^{:doc "Rate-limited version of [[fetch-general-page-data]]"}
  fgpd-slow
  (api-throttler fetch-general-page-data))


(def ^{:doc "Rate-limited version of [[fetch-page-views-data]]"}
  fpvd-slow
  (rest-throttler fetch-page-views-data))


(def ^{:doc "Rate-limited version of [[fetch-page-data]]"}
  fpd-slow
  (api-throttler fetch-page-data))


(defn get-translate-data
  "Fetches translate data for `pages` on a `wiki`. Issues one call
  per 50 pages as per the API limit.

  Uses [[fpd-slow]] to query the Action API, then
  uses [[normalize-translate-data]] to normalize the results."
  [pages wiki]
  (let [batches (partition 50 50 nil pages)
        stringified-batches (map #(clojure.string/join "|" %) batches)
        responses (pmap (fn [b]
                          (-> (fpd-slow b wiki)
                              (normalize-translate-data)))
                        stringified-batches)]
    (vec (flatten responses))))


(defn- fetch-page-views-data-basedon-key
  "Fetches pageview data based on a cache key and name of the page `p`. The key must have the
  following format:
  `:pageviews/<wiki>~~|<cache id>~~|<date from yyyyMMdd>~~|<date to yyyyMMdd>`, for example
  `:pageviews/wikitechwikitechwikimedia~~|-774435264~~|20230407~~|20230706`. Note that `cache id`
  is ignored by this function. It is kept in the key for use in [[get-pageviews-for-page-by-cache-id]].

  For dates, use the UTC-date-now and UTC-date-90 functions. For a list of permitted wikis,
  see [[config/permitted-wikis]]. Page title is automatically escaped inside the function.

  Only use this function for troubleshooting. In normal circumstances, use
  [[fetch-pageviews-through-cache]]."
  [k p]
  (let [id (str/split (name k) #"~~\|")
        wiki (nth id 0)
        page p
        date-90 (nth id 2)
        date-now (nth id 3)]
    (fpvd-slow page wiki date-90 date-now)))


(defn- get-translate-data-basedon-key
  "Fetches translation data based on a cache key. The key must have the following format:
  `:translate/<wiki>~~|<pagepile-id>`, for example
  `:translate/wikitechwikimedia~~|51234`.

  For a list of permitted wikis, see [[config/permitted-wikis]].

  Accepts the following parameters:
  * `k` - cache key
  * `p` - pages"
  [k p]
  (let [id (str/split (name k) #"~~\|")
        wiki (nth id 0)
        pagepile-id (nth id 1)]
    (get-translate-data p wiki)))


(defn fetch-translate-data-through-cache
  "Returns translation information for `pages` on a given `wiki`.
  Uses cache first, with `pagepile-id` used to generate cache key.
  If the data is not available in cache, it issues a call to the API,
  then stores the response in the cache."
  [pagepile-id pages wiki]
  (fetch-cache-record (keyword "translate" (str wiki "~~|" pagepile-id)) #(get-translate-data-basedon-key % pages)))


(defn fetch-pageviews-through-cache
  "Returns pageview information for a given `page` on a given `wiki`. Uses cache first.
  If the data is not available in cache, it issues a call to the API, then stores the response
  in the cache. For a list of supported wikis, see [[config/permitted-wikis]].

  This function uses a rate-limited fetch function and might take a while to run."
  [page wiki]
  (fetch-cache-record (keyword "pageviews" (str wiki "~~|" (hash (str wiki page)) "~~|" (common/UTC-date-90) "~~|" (common/UTC-date-yesterday))) #(fetch-page-views-data-basedon-key % page)))


(defn extract-pageviews-from-response
  "Reformats the pageview response (from cache or API) by removing unnecessary information.
  Makes it easier to process the data further. Takes response `r` returned by the
  [[fetch-pageviews-through-cache]] function as argument."
  [r]
  (reduce (fn [m v] (assoc m (keyword (:timestamp v)) (:views v))) {} r))


(defn get-pageviews-for-page
  "Returns reformatted pageview information for a `page` on a given `wiki`.
  For a list of supported wikis, see [[config/permitted-wikis]]."
  [page wiki]
  (-> page
      (fetch-pageviews-through-cache wiki)
      (extract-pageviews-from-response)))


(defn get-pageviews-for-collection
  "Returns a sorted vector of maps, where each map has the following format:
  `{:date \"yyyyMMdd00\" :value <number of page views>}`

  Takes the following arguments:

  * `pages` should be a simple vector of page titles, for example:
    ```[\"Help:Toolforge\" \"Portal:Cloud VPS\"]```
  * `wiki` should match one of the wikis in [[config/permitted-wikis]]"
  [pages wiki]
  (->> pages
       (reduce (fn [l v] (conj l (get-pageviews-for-page v wiki))) [(common/dates-structure)])
       (common/calculate-sum)
       seq
       (map (fn [r] {:date (name (first r)) :value (last r)}))
       (sort-by :date)
       (into [])))


(defn get-pageviews-for-page-by-cache-id
  "Returns a sorted vector of maps, where each map has the following format:
  `{:date \"yyyyMMdd00\" :value <number of page views>}`. Results are only
  available if page views for the page identified by `cache-id` have already been
  loaded to cache when fetching page views for entire collection. Returns
  zeros for the entire date range if results are not available.

  Takes the following arguments:

  * `cache-id` should be a result of running `(hash (str \"<wiki>\" \"<page name>\")`, for example:
    `-2116991548`
  * `wiki` should match one of the wikis in [[config/permitted-wikis]]"
  [cache-id wiki]
  (->> (keyword "pageviews" (str wiki "~~|" cache-id "~~|" (common/UTC-date-90) "~~|" (common/UTC-date-yesterday)))
       (just-fetch-record)
       (extract-pageviews-from-response)
       (conj (common/dates-structure))
       (map (fn [r] {:date (name (first r)) :value (last r)}))
       (sort-by :date)
       (into [])))


(defn get-translate-data-for-collection
  "Returns a map with two key-value pairs:

  * :rows - a vector of collection pages in the following format:
  `{:page <title> :translate <true|false|nil>}`. The value for `:translate`
  indicates whether the page contains translation syntax. `nil` indicates
  that the page has no content (might be e.g. a redirect).
  * :stats - percentage of pages with translation syntax in the collection."
  [pagepile pages wiki]
  (let [rows (fetch-translate-data-through-cache pagepile pages wiki)
        row-count (count rows)
        rows-translate (count (filter #(true? (:translate %)) rows))
        stats (when (pos? row-count)
                (format "%.2f"
                        (* 100 (/ (float rows-translate) (float row-count)))))]
    {:rows (sort-by :page rows)
     :stats stats}))
