(ns techdoc.dashboard.lib.specs
  (:require
    [clojure.spec.alpha :as s]))


(set! *warn-on-reflection* true)


(s/def :pagepile-id/valid
  (s/and
    string?
    #(re-find #"^\d+$" %)
    #(>= (Integer. ^String %) 0)
    #(< (Integer. ^String %) 1000000)))
