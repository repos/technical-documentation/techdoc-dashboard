(ns techdoc.dashboard.config
  (:require
    [kit.config :as config]))


(set! *warn-on-reflection* true)


(def ^:const system-filename "system.edn")


(def ^{:doc "Service source code license URL"}
  license-url
  "https://gitlab.wikimedia.org/repos/technical-documentation/techdoc-dashboard/-/raw/main/LICENSE")


(def ^{:doc "Service source code repository URL"}
  repo-url
  "https://gitlab.wikimedia.org/repos/technical-documentation/techdoc-dashboard/-/tree/main?ref_type=heads")


(def ^{:doc "Documentation URL"}
  docs-url
  "https://www.mediawiki.org/wiki/Documentation/Tools/Documentation_metrics_dashboard")


(def ^{:doc "Currently supported languages.
       `:src` specifies the location of the translations file,
       `:dir` specifies text direction"}
  supported-languages
  {:en {:src "locale/en.json" :dir "ltr"}
   :pl {:src "locale/pl.json" :dir "ltr"}})


(defn system-config
  "Reads configuration from the configuration file specified in [[system-filename]]."
  [options]
  (config/read-config system-filename options))


(def dashboard-config
  "Configures the application based on the file read via
  [[system-config]]."
  (:techdoc.dashboard/config (system-config nil)))


(def ^{:doc "User agent added to the header of all outgoing API requests.
            Add your contact information in resources/system.edn or in the
            USER_AGENT environment variable to ensure your requests are not blocked."}
  user-agent
  (:user-agent dashboard-config))


(def ^{:doc "Time after which records in cache expire.
             Specify using the CACHE_TTL environment variable or change
             the default value in resources/system.edn. Defaults to 8h.
             Only integers are permitted."}
  cache-ttl
  (Integer. ^String (:cache-ttl dashboard-config)))


(def replica-user
  "User name for accessing replicas, loaded from [[dashboard-config]]"
  (:db-user dashboard-config))


(def replica-password
  "Password used for accessing replicas, loaded from [[dashboard-config]]"
  (:db-password dashboard-config))


(def wikitech-db
  "Wikitech database replica URL, loaded from [[dashboard-config]]"
  (:wikitech-db dashboard-config))


(def mediawiki-db
  "Mediawiki database replica URL, loaded from [[dashboard-config]]"
  (:mediawiki-db dashboard-config))


(def meta-db
  "Meta database replica URL, loaded from [[dashboard-config]]"
  (:meta-db dashboard-config))


(def max-pagepile-size
  (Integer. ^String (:max-pagepile-size dashboard-config)))


(defn generate-db-config
  "Template for generating replica database configuration used to run SQL queries"
  [name user password host]
  {:dbtype "mariadb"
   :dbname name
   :user user
   :password password
   :host host
   :read-only true
   :timeout 300000
   :connectTimeout 20000
   :loginTimeout 20000
   :max-rows 10})


(def ^{:doc "Used to store and reuse Wikitech configuration to reduce duplication in [[permitted-wikis]]."}
  wikitech-struct
  {:id "wikitech.wikimedia.org"
   :article "https://wikitech.wikimedia.org/wiki/"
   :api "https://wikitech.wikimedia.org/w/api.php"
   :rest "https://wikitech.wikimedia.org/w/rest/"
   :db (generate-db-config "labswiki_p" replica-user replica-password wikitech-db)
   :templates ["Archive" "Draft" "Draft-section" "Fixme" "Fixme_inline"  "Historical" "Outdated" "Outdated-inline"]})


(def ^{:doc "List of wikis that can be analyzed using this instance of the dashboard."}
  permitted-wikis
  {"wikitechwikitechwikimedia" wikitech-struct

   "wikitechwikimedia" wikitech-struct

   "mediawikiwiki" {:id "mediawiki.org"
                    :article "https://mediawiki.org/wiki/"
                    :api "https://mediawiki.org/w/api.php"
                    :rest "https://mediawiki.org/w/rest/"
                    :db (generate-db-config "mediawikiwiki_p" replica-user replica-password mediawiki-db)
                    :templates ["DoNotTranslate" "Draft" "Draft-section" "Fixme" "Historical" "Outdated" "Outdated2" "Update"]}

   "metawiki" {:id "meta.wikimedia.org"
               :article "https://meta.wikimedia.org/wiki/"
               :api "https://meta.wikimedia.org/w/api.php"
               :rest "https://meta.wikimedia.org/w/rest/"
               :db (generate-db-config "metawiki_p" replica-user replica-password meta-db)
               :templates ["DoNotTranslate" "Draft" "Draft-section" "Historical" "Outdated"]}})
