(ns techdoc.dashboard.web.htmx
  (:require
    [hiccup.page :as p]
    [hiccup2.core :as h]
    [ring.util.http-response :as http-response]
    [techdoc.dashboard.config :as config]
    [techdoc.dashboard.web.controllers.locale :refer [translate get-language-dir]]))


(set! *warn-on-reflection* true)


(defn base
  "Base template for all UI content displayed in the browser"
  [l content]
  [:head
   [:meta {:charset "UTF-8"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:title (translate l :app-name)]
   [:link {:rel "preconnect" :href "https://tools-static.wmflabs.org"}]
   [:link {:href "https://tools-static.wmflabs.org/fontcdn/css?family=Atkinson+Hyperlegible:400,700&subset=latin,latin-ext&display=swap" :rel "stylesheet" :type "text/css"}]
   [:script {:defer true :src "https://tools-static.wmflabs.org/cdnjs/ajax/libs/htmx/2.0.0-beta2/htmx.min.js" :type "text/javascript"}]
   (p/include-css "/css/app.css")
   [:body.body
    [:div.app
     [:header.nav
      [:a.home {:href (str "/?lang=" (name l)) :role "heading" :aria-level "1"} (translate l :app-name)]
      [:nav
       [:a.language-link {:href "?lang=en"} "en"]
       [:a.language-link {:href "?lang=pl"} "pl"]]]
     [:main content]
     [:footer
      [:p (translate l :footer-text
                     (str (h/html [:a.link {:href "https://creativecommons.org/publicdomain/zero/1.0/"} "CC0"]))
                     (str (h/html [:a.link {:href config/license-url} (translate l :license)]))
                     (str (h/html [:a.link {:href config/repo-url} (translate l :repo)]))
                     (str (h/html [:a.link {:href config/docs-url} (translate l :documentation)]))
                     (str (h/html [:a.link {:href "https://neurodiversity.design/"} "Neurodiversity Design System by Will Soward"])))]]]]])


(defn page
  "Generates page HTML using Hiccup. Use when rendering the entire page."
  [l content]
  (-> (p/html5 {:dir (get-language-dir l) :lang l} (base l content))
      http-response/ok
      (http-response/content-type "text/html")))


(defn ui
  "Generates page element HTML using Hiccup. Use when rendering parts of a page."
  [content]
  (-> (str (h/html content))
      http-response/ok
      (http-response/content-type "text/html")))
