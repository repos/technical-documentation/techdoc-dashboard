(ns techdoc.dashboard.web.routes.ui
  (:require
    [integrant.core :as ig]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.parameters :as parameters]
    [techdoc.dashboard.web.middleware.exception :as exception]
    [techdoc.dashboard.web.middleware.formats :as formats]
    [techdoc.dashboard.web.ui.dashboard :as ui-dashboard]
    [techdoc.dashboard.web.ui.home :as ui-home]
    [techdoc.dashboard.web.ui.partial :as ui-partial]
    [techdoc.dashboard.web.ui.reset :as ui-reset]))


(set! *warn-on-reflection* true)


;; Routes
(defn ui-routes
  [_opts]
  [["/" {:get ui-home/home-page}]
   ["/ui/:pagepile-id/:element-id" {:get ui-partial/get-element}]
   ["/load" {:post ui-home/load-pile}]
   ["/load-nojs" {:post ui-home/home-page-no-js}]
   ["/dashboard/:pagepile-id" {:get ui-dashboard/dashboard}]
   ["/dashboard/:pagepile-id/:page-name" {:get ui-dashboard/page-dashboard}]
   ["/reset" {:get ui-reset/reset}]])


(def route-data
  {:muuntaja   formats/instance
   :middleware
   [;; Default middleware for ui
    ;; query-params & form-params
    parameters/parameters-middleware
    ;; encoding response body
    muuntaja/format-response-middleware
    ;; exception handling
    exception/wrap-exception]})


(derive :reitit.routes/ui :reitit/routes)


(defmethod ig/init-key :reitit.routes/ui
  init-key--reitit.routes-ui
  [_ {:keys [base-path]
      :or   {base-path ""}
      :as   opts}]
  [base-path route-data (ui-routes opts)])
