(ns techdoc.dashboard.web.controllers.pagepile
  "Used to interact with the PagePile API"
  (:require
    [cheshire.core :refer [parse-string]]
    [clj-http.client :as client]
    [clojure.spec.alpha :as s]
    [clojure.string :as str]
    [clojure.tools.logging :as log]
    [hiccup2.core :as h]
    [techdoc.dashboard.config :refer [permitted-wikis user-agent max-pagepile-size]]
    [techdoc.dashboard.lib.specs]
    [techdoc.dashboard.web.controllers.cache :refer [fetch-cache-record just-fetch-record]]
    [techdoc.dashboard.web.controllers.locale :refer [translate]]))


(set! *warn-on-reflection* true)


(defn load-pagepile
  "Takes an `ID` and queries the pagepile API to fetch data for the pile indicated by the ID"
  [id]
  (try (client/get (format "https://pagepile.toolforge.org/api.php?id=%s&action=get_data&doit&format=json&metadata=1"
                           id)
                   {:headers {"User-Agent" user-agent}}
                   {:as :json})
       (catch clojure.lang.ExceptionInfo e
         (let [status (:status (.getData ^clojure.lang.ExceptionInfo e))
               reason (:reason-phrase (.getData ^clojure.lang.ExceptionInfo e))]
           {:status status :body reason}))))


(defn load-pagepile-basedon-keyword
  "Takes a `key` that includes the PagePile ID as its last part (name), and runs load-pagepile using that ID"
  [key]
  (load-pagepile (name key)))


(defn load-pagepile-through-cache
  "Takes an `id` and loads data related to the PagePile with this ID from cache. If it's not found,
  it runs load-pagepile-basedon-keyword using a keyword constructed from this ID and stores its returned
  data in cache"
  [id]
  (log/info (str "Getting pagepile: " id " through cache"))
  (fetch-cache-record (keyword "pagepile" (str id)) load-pagepile-basedon-keyword))


(defn load-pagepile-from-cache
  "Takes an `id` and loads data related to the PagePile with this ID from cache. If it's not found,
  it returns a data structure with status set to `-1` that is handled during validation."
  [id]
  (log/info (str "Getting pagepile " id " from cache"))
  (let [jfr (just-fetch-record (keyword "pagepile" (str id)))]
    (if (nil? jfr)
      {:status -1}
      jfr)))


(defn validate-pagepile
  "Validates PagePile API server response provided by `load-pagepile`
  and returns a data structure with pages in the pile (if the response is correct),
  or the error message (if the response indicates any errors)"
  [{:keys [body status]} l]
  (cond
    (= status -1) {:status :error :body (translate l :pagepile-cache-failed)}
    (not= status 200) {:status :error :body (translate l :pagepile-cannot-connect) :original-body body}
    (or (str/includes? body "Uncaught Exception")
        (str/includes? body "Uncaught Error")) {:status :error :body (translate l :pagepile-invalid-id)}
    (not (contains? permitted-wikis (get (parse-string body) "wiki"))) {:status :error :body (translate l :pagepile-unsupported-wiki)}
    :else {:status :ok :body (get (parse-string body) "pages") :wiki (get (parse-string body) "wiki")}))


(defn get-pagepile-structured
  "Fetches and validates the response of a request to PagePile API.
  The request is for the contents of a pile indicated by `ID`.

  Unlike [[get-pagepile]], returns a simple data structure that must
  be processed before displaying.
  "
  [id l sec]
  (let [pagepile-response (if (s/valid? :pagepile-id/valid id)
                            (-> (if sec
                                  (load-pagepile-from-cache id)
                                  (load-pagepile-through-cache id))
                                (validate-pagepile l))
                            {:status :error :body (translate l :pagepile-invalid-id)})]

    (if
      (= (:status pagepile-response) :ok)
      {:pages (->> (into [] (take max-pagepile-size) (keys (:body pagepile-response)))
                   (sort))
       :wiki (:wiki pagepile-response) :error false}
      {:pages nil :wiki nil :error true :error-message (:body pagepile-response)})))
