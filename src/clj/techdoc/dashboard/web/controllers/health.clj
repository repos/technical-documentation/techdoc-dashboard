(ns techdoc.dashboard.web.controllers.health
  (:require
    [ring.util.http-response :as http-response])
  (:import
    (java.util
      Date)))


(set! *warn-on-reflection* true)


(defn healthcheck!
  [req]
  (http-response/ok
    {:time     (str (Date. (System/currentTimeMillis)))
     :up-since (str (Date. (.getStartTime (java.lang.management.ManagementFactory/getRuntimeMXBean))))
     :app      {:status  "up"
                :message ""}}))
