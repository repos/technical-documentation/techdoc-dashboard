(ns techdoc.dashboard.web.controllers.cache
  "Used to interact with cache."
  (:require
    [clojure.core.cache.wrapped :as w]
    [techdoc.dashboard.config :as config]))


(set! *warn-on-reflection* true)


(def ^{:doc "Atom that holds the cache. Records expire after the number of hours specified in the CACHE_TTL environment variable, or 8 hours (if unspecified). See [[config/cache-ttl]] for details."}
  C
  (w/ttl-cache-factory {} :ttl (* config/cache-ttl 3600000)))


(defn fetch-cache-record
  "Wrapper around lookup-or-miss. For key `k` and function `f`, executes function `f` if the record with key `k` is not found in cache"
  [k f]
  (w/lookup-or-miss C k f))


(defn just-fetch-record
  "For key `k`, returns a record in cache with that key, or nil if it's not found"
  [k]
  (w/lookup C k))
