(ns techdoc.dashboard.web.ui.dashboard
  (:require
    [clojure.tools.logging :as log]
    [techdoc.dashboard.lib.mw-client :as mw]
    [techdoc.dashboard.lib.replica-client :as replica]
    [techdoc.dashboard.web.controllers.locale :refer [tr]]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.htmx :refer [page] :as htmx]
    [techdoc.dashboard.web.ui.widgets :as widgets]))


(set! *warn-on-reflection* true)


(defn dashboard
  "Displays the collection dashboard page. Takes pagepile-id to display
  data from path. Takes language from params (```?lang=x```) and defaults to \"en\".

  Depending on the size of the pile, might take a long time to run. Runs all
  calculations to display in every widget."
  [{{:keys [pagepile-id]} :path-params
    {:keys [lang]
     :or {lang "en"}} :params}]
  (let [p (pagepile/get-pagepile-structured pagepile-id (keyword lang) true)]
    (log/info "Load dashboard for pagepile: " pagepile-id)
    (if (:error p)

      (page
        (keyword lang)
        [:div.card
         [:p (str (tr lang :dashboard-error) " ")
          [:a.link {:href "/"} (tr lang :dashboard-startover)]]])

      (let [pageviews-for-collection (future (mw/get-pageviews-for-collection (:pages p) (:wiki p)))
            translate-data-for-collection (future (mw/get-translate-data-for-collection pagepile-id (:pages p) (:wiki p)))
            edits-for-collection (replica/get-revisions-90d pagepile-id (:pages p) (:wiki p))
            top-editors-for-collection (replica/get-editors-90d pagepile-id (:pages p) (:wiki p))
            revision-sizes-for-collection (replica/get-revisionsizes-90d pagepile-id (:pages p) (:wiki p))
            templates-for-collection (replica/get-templates-current pagepile-id (:pages p) (:wiki p))]
        (page
          (keyword lang)
          [:div.card
           [:h2 (str (tr lang :dashboard-title) " " pagepile-id)]
           [:div.grid
            (widgets/pageviews-widget (tr lang :pageviews-widget-title)
                                      @pageviews-for-collection
                                      true
                                      lang
                                      pagepile-id
                                      pagepile-id)
            (widgets/pageedits-widget (tr lang :pageedits-widget-title)
                                      edits-for-collection
                                      true
                                      lang
                                      pagepile-id
                                      pagepile-id)
            (widgets/revisionsizes-widget (tr lang :revisionsizes-widget-title)
                                          revision-sizes-for-collection
                                          true
                                          lang)
            (widgets/translate-widget (tr lang :translate-widget-title)
                                      @translate-data-for-collection
                                      lang
                                      pagepile-id
                                      pagepile-id)
            (widgets/topeditors-widget (tr lang :topeditors-widget-title)
                                       top-editors-for-collection
                                       lang
                                       pagepile-id
                                       pagepile-id)
            (widgets/templates-widget (tr lang :template-widget-title)
                                      templates-for-collection
                                      lang
                                      pagepile-id
                                      pagepile-id)
            (widgets/page-list-widget (tr lang :pagelist-widget-title)
                                      lang
                                      (:pages p)
                                      pagepile-id)]])))))


(defn page-dashboard
  "Displays the page dashboard. Takes `pagepile-id` and `page-name` to display
  data from path. Takes language from params (```?lang=x```) and defaults to \"en\".

  Only available for pages from page piles that have already been loaded by
  [[dashboard]]."
  [{{:keys [pagepile-id page-name]} :path-params
    {:keys [lang]
     :or {lang "en"}} :params}]
  (let [p (pagepile/get-pagepile-structured pagepile-id (keyword lang) true)
        wiki (:wiki p)
        pages (when (some #(= page-name %) (:pages p)) [page-name])
        cache-id (hash (str wiki page-name))]
    (log/info (str "Load dashboard for page: " page-name ", from page pile: " pagepile-id))
    (if (or (:error p) (nil? pages))
      (page
        (keyword lang)
        [:div.card
         [:p (str (tr lang :dashboard-error) " ")
          [:a.link {:href "/"} (tr lang :dashboard-startover)]]])
      (let [pageviews-for-collection (future (mw/get-pageviews-for-page-by-cache-id cache-id wiki))
            translate-data-for-collection (future (mw/get-translate-data-for-collection cache-id pages wiki))
            edits-for-collection (replica/get-revisions-90d cache-id pages wiki)
            top-editors-for-collection (replica/get-editors-90d cache-id pages wiki)
            revision-sizes-for-collection (replica/get-revisionsizes-90d cache-id pages wiki)
            templates-for-collection (replica/get-templates-current cache-id pages wiki)]

        (page
          (keyword lang)
          [:div.card
           [:h2 (str (tr lang :page-dashboard-title) " " page-name)]
           [:a.link.btn.backlink
            {:href (str "/dashboard/" pagepile-id "?lang=" lang)} (tr lang :page-dashboard-back-button)]

           [:div.grid
            (widgets/pageviews-widget (tr lang :page-pageviews-widget-title)
                                      @pageviews-for-collection
                                      true
                                      lang
                                      pagepile-id
                                      cache-id)
            (widgets/pageedits-widget (tr lang :page-pageedits-widget-title)
                                      edits-for-collection
                                      true
                                      lang
                                      pagepile-id
                                      cache-id)
            (widgets/revisionsizes-widget (tr lang :revisionsizes-widget-title)
                                          revision-sizes-for-collection
                                          true
                                          lang)
            (widgets/translate-widget (tr lang :translate-widget-title)
                                      @translate-data-for-collection
                                      lang
                                      pagepile-id
                                      cache-id)
            (widgets/topeditors-widget (tr lang :topeditors-widget-title)
                                       top-editors-for-collection
                                       lang
                                       pagepile-id
                                       cache-id)
            (widgets/templates-widget (tr lang :template-widget-title)
                                      templates-for-collection
                                      lang
                                      pagepile-id
                                      cache-id)]])))))
