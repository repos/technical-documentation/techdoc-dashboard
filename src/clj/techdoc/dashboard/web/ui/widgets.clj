(ns techdoc.dashboard.web.ui.widgets
  (:require
    [techdoc.dashboard.lib.chart-generator :as ch]
    [techdoc.dashboard.web.controllers.locale :refer [tr]]
    [techdoc.dashboard.web.ui.tables :as tables]))


(set! *warn-on-reflection* true)


(defn pageviews-widget
  "Displays the widget with page view data for the collection.

  Expects the following parameters:
  * `title` - displayed in the header
  * `pageviews-for-collection` - pageviews data returned by `mw/get-pageviews-for-collection`
  * `display-chart` - boolean value to indicate whether to display a chart
  * `lang` - language
  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache. On the collection
  dashboard, `cache-id` is typically the same as `pagepile-id`
  "
  [title pageviews-for-collection display-chart lang pagepile-id cache-id]
  (let [pageview-values-vector (vec (map :value pageviews-for-collection))]
    [:div.widget nil
     [:div.title [:h3 {} title]]
     (when display-chart  [:div.chart-container nil [:div.chart-box nil (ch/chart->svg (ch/generate-bar-chart pageview-values-vector (tr lang :pageviews-chart-title) (tr lang :pageviews-chart-y) (tr lang :pageviews-chart-x)))]])
     [:div.table-container {} (tables/table-for-pageviews pageviews-for-collection lang pagepile-id cache-id)]]))


(defn pageedits-widget
  "Displays the widget with page edit data for the collection.

  Expects the following parameters:

  * `title` - displayed in the header
  * `edits` - edits data returned by `replica/get-revisions-90d`
  * `display-chart` - boolean value to indicate whether to display a chart
  * `lang` - language
  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache. On the collection
  dashboard, `cache-id` is typically the same as `pagepile-id`."
  [title edits display-chart lang pagepile-id cache-id]
  (let [edits-vector (vec (map :value edits))]
    [:div.widget nil
     [:div.title [:h3 {} title]]
     (when display-chart  [:div.chart-container nil [:div.chart-box nil (ch/chart->svg (ch/generate-bar-chart edits-vector (tr lang :pageedits-chart-title) (tr lang :pageedits-chart-y) (tr lang :pageedits-chart-x)))]])
     [:div.table-container {} (tables/table-for-edits edits lang pagepile-id cache-id)]]))


(defn topeditors-widget
  "Displays the widget with page editor data for the collection.

  Expects the following parameters:

  * `title` - displayed in the header
  * `edits` - editor data returned by `replica/get-editors-90d`
  * `lang` - language
  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache. On the collection
  dashboard, `cache-id` is typically the same as `pagepile-id`"
  [title editors lang pagepile-id cache-id]
  [:div.widget nil
   [:div.title [:h3 {} title]]
   [:div.table-container {} (tables/table-for-editors editors lang pagepile-id cache-id)]])


(defn revisionsizes-widget
  "Displays the widget with revision size data for the collection.

  Expects the following parameters:

  * `title` - displayed in the header
  * `revisionsizes` - data returned by `replica/get-revisionsizes-90d`
  * `lang` - language"
  [title revisionsizes display-chart lang]
  [:div.widget nil
   [:div.title [:h3 {} title]]
   (when display-chart  [:div.chart-container nil [:div.chart-box nil (ch/chart->svg (ch/generate-sizes-chart revisionsizes (tr lang :revisionsizes-widget-title) (tr lang :revisionsizes-chart-editnumber) (tr lang :revisionsizes-chart-label-a) (tr lang :revisionsizes-chart-label-b) (tr lang :revisionsizes-chart-label-c) (tr lang :revisionsizes-chart-label-d)))]])
   [:div.table-container {} (tables/table-for-revision-sizes revisionsizes lang)]])


(defn templates-widget
  "Displays the widget with template data for the collection.

  Expects the following parameters:

  * `title` - displayed in the header
  * `templates` - template data returned by `replica/get-templates-current`
  * `lang` - language
  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache. On the collection
  dashboard, `cache-id` is typically the same as `pagepile-id`"
  [title templates lang pagepile-id cache-id]
  [:div.widget nil
   [:div.title [:h3 {} title]]
   [:div.table-container {} (tables/table-for-templates templates lang pagepile-id cache-id)]])


(defn translate-widget
  "Displays the widget with translation data for the collection.

  Expects the following parameters:

  * `title` - displayed in the header
  * `translate-data` - data returned by `mw/get-translate-data-for-collection`
  * `lang` - language
  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache. On the collection
  dashboard, `cache-id` is typically the same as `pagepile-id`"
  [title translate-data lang pagepile-id cache-id]
  [:div.widget nil
   [:div.title [:h3 {} title]]
   [:div {} [:p (str (tr lang :translate-widget-percentage) " " (:stats translate-data) "%")]]
   [:div.table-container {} (tables/table-for-translate-data (:rows translate-data) lang pagepile-id cache-id)]])


(defn page-list-widget
  "Displays a widget with a list of pages in the pile. Each list item links
  to a dashboard with data for this page.

  Expects the following parameters:

  * `title` - displayed in the header
  * `lang` - language
  * `pages` - list of pages returned by `get-pagepile-structured`
  * `pagepile-id`"
  [title lang pages pagepile-id]
  [:div.widget nil
   [:div.title [:h3 {} title]]
   [:ul nil (map (fn [p]
                   [:li {}
                    [:a {:href (str "/dashboard/" pagepile-id "/" (java.net.URLEncoder/encode p) "?lang=" lang)} (str p)]]) pages)]])
