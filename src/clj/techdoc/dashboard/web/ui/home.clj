(ns techdoc.dashboard.web.ui.home
  (:require
    [clojure.string :refer [blank?]]
    [hiccup2.core :as h]
    [techdoc.dashboard.config :refer [permitted-wikis]]
    [techdoc.dashboard.web.controllers.locale :refer [translate tr] :as l]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.htmx :refer [ui page] :as htmx]))


(set! *warn-on-reflection* true)


(defn button
  "Displays a form submission button"
  [{:keys [label disabled type]
    :or {label (translate :en :button-submit),
         disabled false,
         type "submit",}}]
  [:button.btn {:type type :disabled disabled} label])


(defn home-page-ui
  "Framework for the home page. Used as a starting point for JS and non-JS version of the page"
  [l inner-content]
  [:div.card
   [:p (translate l :specify-pagepile)]
   [:form
    {:action (str "/load-nojs?lang=" (name l)) :method "POST" :hx-post (str "/load?lang=" (name l)) :hx-target "#pile-content" :hx-swap "outerHTML" :hx-indicator "#spinner"}
    [:label {:for "pagepile"} (str (translate l :pagepile-id) ":")]
    [:input {:type "number" :id "pagepile" :name "pagepile" :required true}]
    [:label.invisible {:for "name" :aria-hidden "true"} "Name"]
    [:input.invisible {:type "text" :id "name" :name "name" :aria-hidden "true" :autocomplete "off"}] ; honeypot field
    (button {:label (translate l :button-load)})]
   [:div.pagepile-container inner-content]])


(defn home-page
  "Starting page displayed regardless of whether JS is enabled or not"
  [{{:keys [lang]
     :or {lang "en"}} :params}]
  (page
    (keyword lang)
    (home-page-ui (keyword lang) [:div#pile-content {}])))


(defmulti format-pagepile-contents
  "Formats the PagePile API response returned by get-pagepile-structured as hiccup.
  The response contains a list of pages in the given pile in case of success,
  and an error message if an error occurred."
  :error)


(defmethod format-pagepile-contents false
  ;; No errors
  format-pagepile-contents-noerrors
  [{:keys [pages wiki]}]
  (let [wiki-url (get-in permitted-wikis [wiki :article])]
    [:ul (->> pages
              (map (fn [p]
                     [:li {}
                      [:a {:href (str (h/raw wiki-url p))} (str (h/raw p))]])))]))


(defmethod format-pagepile-contents true
  ;; Errors
  format-pagepile-contents--errors
  [{:keys [error-message]}]
  [:div {} [:p error-message]])


(defn home-page-no-js
  "Starting page with loaded PagePile contents. Only displayed when JS is not available in the browser"
  [{{:keys [pagepile lang name]
     :or {lang "en"}} :params}]
  (let [pagepile-response (if (blank? name)
                            (pagepile/get-pagepile-structured pagepile (keyword lang) false)
                            {:error true :error-message (tr lang :pagepile-bot-detected)})
        errors-occurred (:error pagepile-response)]
    (page (keyword lang)
          (home-page-ui (keyword lang) [:div#pile-content
                                        (when (false? errors-occurred)
                                          [:h2 (tr lang :pagepile-contents)]
                                          [:div.pile-content-question {}
                                           [:p (tr lang :download-statistics-q) " "]
                                           [:a.btn {:href (str "/dashboard/" pagepile "?lang=" lang) :role "button"} (tr lang :open-pagepile-stats)]])
                                        [:div
                                         [:div {} (format-pagepile-contents pagepile-response)]]]))))


(defn load-pile
  "Partial with PagePile contents. Loaded onto home page only when JS is available in the browser."
  [{{:keys [pagepile lang name]
     :or {lang "en"}} :params}]
  (let [pagepile-response (if (blank? name)
                            (pagepile/get-pagepile-structured pagepile (keyword lang) false)
                            {:error true :error-message (tr lang :pagepile-bot-detected)})
        errors-occurred (:error pagepile-response)]
    (ui
      [:div#pile-content
       [:span#spinner.htmx-indicator (tr lang :loading)]
       (when (false? errors-occurred)
         [:h2 (tr lang :pagepile-contents)]
         [:div.pile-content-question {}
          [:p (tr lang :download-statistics-q) " "]
          [:a.btn {:href (str "/dashboard/" pagepile "?lang=" lang)} (tr lang :open-pagepile-stats)]])
       [:div.pile-content-list {} (format-pagepile-contents pagepile-response)]])))
