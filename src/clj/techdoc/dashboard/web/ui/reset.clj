(ns techdoc.dashboard.web.ui.reset
  (:require
    [techdoc.dashboard.web.htmx :refer [page] :as htmx]))


(set! *warn-on-reflection* true)


(defn reset
  [request]
  (page :en
        [:div "Cache reset"]))
