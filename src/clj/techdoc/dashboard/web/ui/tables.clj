(ns techdoc.dashboard.web.ui.tables
  (:require
    [techdoc.dashboard.web.controllers.locale :refer [tr]]))


(set! *warn-on-reflection* true)


(defn sorting-buttons
  "Generates sorting buttons used in table headings. Each
  button uses a GET request to call the `get-element`
  function that returns the HTML of a sorted table.

  Parameters:

  * `pagepile-id`
  * `cache-id` - unique identifier of data stored in cache
  * `lang`
  * `element` - data structure and table to be sorted
  * `target` - DOM identifier for the table that will be updated
  * `column` - column to use for sorting
  "
  [pagepile-id cache-id lang element target column]
  [:span.sorting-buttons nil
   [:button {:hx-get (str "/ui/" pagepile-id "/" element "?lang=" lang "&col=" column "&ord=asc&cid=" cache-id)
             :hx-target target
             :hx-swap "outerHTML"
             :aria-label (tr lang (keyword (str "sort-by-" column "-ascending")))} "▲"]
   [:button {:hx-get (str "/ui/" pagepile-id "/" element "?lang=" lang "&col=" column "&ord=desc&cid=" cache-id)
             :hx-target target
             :hx-swap "outerHTML"
             :aria-label (tr lang (keyword (str "sort-by-" column "-descending")))} "▼"]])


(defn sort-table-data
  " Sorts a data structure returned by `replica` or
  `mw-client`.

  Correct values for the `order` parameter: `:asc`, `:desc`

  Correct values for the `column` parameter:

  * for editors: `:editor`, `:edits`
  * for pageedits: `:date`, `:value`
  * for pageviews: `:date`, `:value`
  * for translate data table: `:page`, `:translate`
  * for templates table: `:template`, `:page`
  "
  [data column order]
  (if-not (= :none column)
    (let [sorted (sort-by column data)]
      (if (= order :desc)
        (reverse sorted)
        sorted))
    data))


(defn table-for-pageviews
  "Generates a sorted table of records for page views.

  `pageviews-for-collection` - vector of page views in which
  every record has the following format:
  `{:date \"yyyyMMdd00\" :value <number of views>}`

  Other parameters are used for sorting, labels, and for generating
  sorting links."
  ([pageviews-for-collection lang pagepile-id cache-id] (table-for-pageviews pageviews-for-collection lang pagepile-id cache-id :date :asc))
  ([pageviews-for-collection lang pagepile-id cache-id column order]
   (let [sorted-data (sort-table-data pageviews-for-collection column order)
         element-to-rerender (if (= pagepile-id cache-id) "collection-views" "page-views")
         tbody (->> sorted-data
                    (map (fn [r] [:tr nil [:td nil (subs (:date r) 0 8)] [:td nil (str (:value r))]]))
                    (into [:tbody nil]))]
     [:table#pageviews-table nil [:thead nil
                                  [:tr
                                   [:th
                                    (tr lang :dashboard-date)
                                    (sorting-buttons pagepile-id cache-id lang element-to-rerender "#pageviews-table" "date")]
                                   [:th
                                    (tr lang :dashboard-views)
                                    (sorting-buttons pagepile-id cache-id lang element-to-rerender "#pageviews-table" "value")]]]
      tbody])))


(defn table-for-edits
  "Generates a sorted table of records for page revisions.

  `edits` - vector of page edits in which
  every record has the following format:
  `{:date \"yyyyMMdd00\" :value <number of edits>}`

  Other parameters are used for sorting, labels, and for generating
  sorting links."
  ([edits lang pagepile-id cache-id] (table-for-edits edits lang pagepile-id cache-id :date :asc))
  ([edits lang pagepile-id cache-id column order]
   (let [sorted-data (sort-table-data edits column order)
         tbody (->> sorted-data
                    (map (fn [r] [:tr nil [:td nil (subs (:date r) 0 8)] [:td nil (str (:value r))]]))
                    (into [:tbody nil]))]
     [:table#pageedits-table nil [:thead nil
                                  [:tr
                                   [:th
                                    (tr lang :dashboard-date)
                                    (sorting-buttons pagepile-id cache-id lang "edits" "#pageedits-table" "date")]
                                   [:th
                                    (tr lang :dashboard-edits)
                                    (sorting-buttons pagepile-id cache-id lang "edits" "#pageedits-table" "value")]]]
      tbody])))


(defn table-for-editors
  "Displays a table for a vector of editor records, generating
  a row for each editor. Expects two parameters:

  `editors` - vector of editors, where each editor record has the
  following format: {:editor <name> :edits <number>}

  Other parameters are used for sorting, labels, and for generating
  sorting links."
  ([editors lang pagepile-id cache-id] (table-for-editors editors lang pagepile-id cache-id :edits :desc))
  ([editors lang pagepile-id cache-id column order]
   (let [sorted-data (sort-table-data editors column order)
         tbody (into [:tbody nil] (map (fn [editor] [:tr nil [:td (:editor editor)] [:td (:edits editor)]]) sorted-data))]
     [:table#editors-table
      nil [:thead nil
           [:tr
            [:th (tr lang :topeditors-editor-name)
             (sorting-buttons pagepile-id cache-id lang "editors" "#editors-table" "editor")]
            [:th (tr lang :topeditors-editor-edits)
             (sorting-buttons pagepile-id cache-id lang "editors" "#editors-table" "edits")]]]
      tbody])))


(defn table-for-revision-sizes
  "Displays a table for a map of revision sizes, generating
  four rows: number of big edits (>= 1000), number of small edits (< 1000),
  number of major edits, number of minor edits.

  Expects two parameters:

  * `rs` - a map of revision sizes {:big_edits <number> :small_edits <number>
                                    :major_edits <number> :minor_edits <number>}
  * `lang` - table heading and label language"
  [rs lang]
  [:table {}
   [:tbody {}
    [:tr {}
     [:td {} (tr lang :revisionsizes-bigedits-label)]
     [:td {} (:big_edits rs)]]
    [:tr {}
     [:td {} (tr lang :revisionsizes-smalledits-label)]
     [:td {} (:small_edits rs)]]
    [:tr {}
     [:td {} (tr lang :revisionsizes-majoredits-label)]
     [:td {} (:major_edits rs)]]
    [:tr {}
     [:td {} (tr lang :revisionsizes-minoredits-label)]
     [:td {} (:minor_edits rs)]]]])


(defn table-for-templates
  "Displays a table for a vector of templates, generating
  a row for each record. Expects two parameters:

  * `templates` - vector of page and template combinations,
  where each record has the following format:
  `{:page <name> :template <name>}`"
  ([templates lang pagepile-id cache-id]
   (table-for-templates templates lang pagepile-id cache-id :template :asc))
  ([templates lang pagepile-id cache-id col ord]
   (let [sorted-data (sort-table-data templates col ord)
         tbody (into [:tbody nil] (map (fn [template] [:tr nil [:td (:template template)] [:td (:page template)]]) sorted-data))]
     [:table#template-data-table nil
      [:thead nil
       [:tr
        [:th (tr lang :template-widget-template)
         (sorting-buttons pagepile-id cache-id lang "template" "#template-data-table" "template")]
        [:th (tr lang :template-widget-page)
         (sorting-buttons pagepile-id cache-id lang "template" "#template-data-table" "page")]]]
      tbody])))


(defn table-for-translate-data
  "Displays a table for a vector of translation records, generating
  a row for each page. Expects two parameters:

  * `translate-data` - vector of translation records,
  where each record has the following format:
  `{:page <name> :translate <true|false|nil>}`
  * `lang` - label language"
  ([translate-data lang pagepile-id cache-id]
   (table-for-translate-data translate-data lang pagepile-id cache-id :page :asc))
  ([translate-data lang pagepile-id cache-id col ord]
   (let [sorted-data (sort-table-data translate-data col ord)
         tbody (into [:tbody nil]
                     (map
                       (fn [record]
                         (let [page (:page record)
                               translate (case (:translate record)
                                           nil (tr lang :translate-widget-nocontent)
                                           true (tr lang :translate-widget-true)
                                           false (tr lang :translate-widget-false))]
                           [:tr nil [:td page] [:td translate]]))
                       sorted-data))]
     [:table#translate-data-table
      [:thead nil
       [:tr
        [:th
         (tr lang :translate-widget-page)
         (sorting-buttons pagepile-id cache-id lang "translate" "#translate-data-table" "page")]
        [:th
         (tr lang :translate-widget-translate)
         (sorting-buttons pagepile-id cache-id lang "translate" "#translate-data-table" "translate")]]]
      tbody])))
