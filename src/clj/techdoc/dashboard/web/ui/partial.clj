(ns techdoc.dashboard.web.ui.partial
  (:require
    [techdoc.dashboard.config :as config]
    [techdoc.dashboard.lib.mw-client :as mw]
    [techdoc.dashboard.lib.replica-client :as replica]
    [techdoc.dashboard.web.controllers.locale :refer [tr]]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.htmx :refer [ui] :as htmx]
    [techdoc.dashboard.web.ui.tables :as tables]))


(set! *warn-on-reflection* true)


(def ^{:doc "Permitted values of the `col` parameter."}
  col-values
  ["editor" "edits" "date" "value" "page" "translate" "template"])


(def ^{:doc "Permitted values of the `ord` parameter."}
  ord-values
  ["desc" "asc"])


(def ^{:doc "Permitted values of the `lang` parameter."}
  lang-values
  (map name (keys config/supported-languages)))


(defn incorrect-parameter?
  "Checks if a given parameter is present in a specific
  allowlist and returns `true` if it isn't.
  Use the lists defined at the beginning of
  this file to verify the parameters. For example:

  `(incorrect-parameter? \"test\" lang-values)`"
  [parameter allow-list]
  (nil? (some #{parameter} allow-list)))


(defn incorrect-number?
  "Checks if a given string is not an integer. Used
  when validating pagepile-id and cache-id. Usage:

  `(incorrect-number? \"test\")`"
  [number]
  (try (not (number? (Integer/parseInt number)))
       (catch Exception _
         true)))


(defn get-element
  "Serves as a router for a single endpoint used to display sorted tables.
  Only used for async requests.

  Path parameters:

  * `pagepile-id` - partials are only available for page piles already
  in cache (secure mode)
  * `element-id` - an identifier of the table being sorted. This can be
  \"views\", \"edits\", \"translate\", \"template\", \"editor\".

  URL parameters:

  * `lang` - translations language
  * `col` - column, passed as argument to the sorting function
  * `ord` - ordering, passed as argument to the sorting function
  * `cid` - cache identifier, used to load data from cache on the
  page dashboard.

  Example of a valid URL: /ui/52454/translate?lang=en&col=translate&ord=desc
  "
  [{{:keys [pagepile-id element-id]}                            :path-params
    {:keys [lang col ord cid] :or {lang "en" col "none" ord "asc" cid nil}} :params}]
  (let [pagepile (pagepile/get-pagepile-structured pagepile-id (keyword lang) true)
        error (or (:error pagepile)
                  (incorrect-parameter? lang lang-values)
                  (incorrect-parameter? col col-values)
                  (incorrect-parameter? ord ord-values)
                  (incorrect-number? pagepile-id)
                  (incorrect-number? cid))]
    (ui
      (if
        error
        [:div [:p nil (str (tr lang :dashboard-error) " ")
               [:a.link {:href "/"} (tr lang :dashboard-startover)]]]
        (let [pages (:pages pagepile)
              wiki (:wiki pagepile)
              element-to-rerender (keyword element-id)
              column (keyword col)
              order (keyword ord)
              cache-id (if (nil? cid) pagepile-id cid)
              table (case element-to-rerender
                      :page-views (-> (mw/get-pageviews-for-page-by-cache-id cache-id wiki)
                                      (tables/table-for-pageviews lang pagepile-id cache-id column order))
                      :collection-views (-> (mw/get-pageviews-for-collection pages wiki)
                                            (tables/table-for-pageviews lang pagepile-id cache-id column order))
                      :edits (-> (replica/get-revisions-90d cache-id pages wiki)
                                 (tables/table-for-edits lang pagepile-id cache-id column order))
                      :translate (-> (mw/get-translate-data-for-collection cache-id pages wiki)
                                     (get-in [:rows])
                                     (tables/table-for-translate-data lang pagepile-id cache-id column order))
                      :template (-> (replica/get-templates-current cache-id pages wiki)
                                    (tables/table-for-templates lang pagepile-id cache-id column order))
                      :editors (-> (replica/get-editors-90d cache-id pages wiki)
                                   (tables/table-for-editors lang pagepile-id cache-id column order))
                      [:div [:p nil (str (tr lang :dashboard-error) " ")
                             [:a.link {:href "/"} (tr lang :dashboard-startover)]]])]

          table)))))
