(ns techdoc.dashboard.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[Dashboard starting]=-"))
   :start      (fn []
                 (log/info "\n-=[Dashboard started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[Dashboard has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
