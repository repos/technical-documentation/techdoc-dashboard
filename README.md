# Techdoc Dashboard

* Status: Version 1.0.3 (for original scope, see [T334839](https://phabricator.wikimedia.org/T334839))
* User documentation: https://www.mediawiki.org/wiki/Documentation/Tools/Documentation_metrics_dashboard
* Phabricator tag: https://phabricator.wikimedia.org/tag/documentation-metrics-dashboard/

## Application architecture

This application uses the [Kit framework](https://kit-clj.github.io/). To learn more about its fundamental architecture and design, see the framework's documentation.

## Repository branching model

This repository uses the [git-flow](https://nvie.com/posts/a-successful-git-branching-model/) branching model. The following branches are available:

* `main` - production release branch.
* `develop` - development and integration branch. All new features are merged into this branch first, before being merged to main for production releases. Some development can also happen directly on this branch.
* `feature/*` - feature branches. Used for new functionality.

## Development requirements

Install [Clojure](https://clojure.org/guides/install_clojure).

You can optionally install [babashka](https://babashka.org/). This gives you access to the tasks defined in ``bb.edn``. These tasks are a faster way of running the important Clojure commands.

## Running in development

**Before running the application, be sure to set the USER_AGENT environment variable on your device. Include your SUL account name or other identifying information. Otherwise your API requests might get blocked.**

Start a [REPL](#repl) in your editor or terminal of choice. If you don't know what a REPL is, see this [introduction to REPL](https://clojure.org/guides/repl/introduction).

Start the application with:

```clojure
(go)
```

The application's homepage is available at http://localhost:3000/.

System configuration is available under `resources/system.edn`.

To reload changes:

```clojure
(reset)
```

### Accessing wiki replicas during development

Replica servers are not available from outside Toolforge. This means requests for data from replicas (all edit-related widgets) will fail unless you specifically set up your environment for this purpose. Note that you don't necessarily need data from replicas to run or develop the application. All other functionality should work correctly without it.

To allow the dashboard to access data from a replica database:

1. Set up a tunnel to a replica server as described in [Connecting to the database replicas from your own computer](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Connecting_to_the_database_replicas_from_your_own_computer). Note that you must create the tunnel on port 3306 for it to work with the dashboard.
2. Configure the dashboard to connect to the database through the tunnel. You can do this by setting the following environment variables:
   * `DB_URL_<database name>`, for example `DB_URL_WIKITECH` - should be `localhost`.
   * `TOOL_REPLICA_USER` - database user name available in your replica.my.cnf file.
   * `TOOL_REPLICA_PASSWORD` - database user password available in your replica.my.cnf file.

Note that the dashboard assumes the port for your database connection is 3306, which means you can only connect to a single database instance locally.

## Building and running in a Docker container

**Before running the application, check `resources/system.edn` for information on environment variables that you might want to set (and their defaults). Pass the variable values to the container using the `e` parameter. Be sure to at least set the USER_AGENT**

To build the application inside a Docker container, run:

`docker build -f .pipeline/blubber.yaml --tag run-clj --target run .`

To run the container:

`docker run -e USER_AGENT='<your user agent>' -p 3000:3000 run-clj`

## Building code documentation

Run ``bb codox`` to generate a static HTML representation of code documentation strings. All documentation files are available in ``target/doc``.

## REPL

### Visual Studio Code/VSCodium

Install [Calva](https://calva.io/). Start the REPL in the terminal by running `bb cider` or `clj -M:dev:cider` in the root directory. Then run the Calva command to connect to a running, non-production REPL inside Visual Studio Code. You can also use the jack-in command directly from the editor (without starting the server first), in which case be sure to select `techdoc-dashboard` (or the name of your project directory), then `deps.edn`, and then the `:cider` and `:dev` aliases.

### Cursive

Configure a [REPL following the Cursive documentation](https://cursive-ide.com/userguide/repl.html). Using the default "Run with IntelliJ project classpath" option will let you select an alias from the "Clojure deps" aliases selection.

### Emacs with CIDER

Use the `cider` alias for CIDER nREPL support (run `bb cider` or `clj -M:dev:cider`). See the [CIDER docs](https://docs.cider.mx/cider/basics/up_and_running.html) for more help.

This alias runs nREPL with CIDER during development.

### Command line

Run `bb nrepl`, `clj -M:dev:nrepl`, or `make repl`.

Note that, just like with [CIDER](#cider), this alias runs nREPL during development.

## Managing dependencies

Use [antq](https://github.com/liquidz/antq). Make sure you are running Clojure 1.11.1.1139 or higher and install antq globally based on the [instructions available on GitHub](https://github.com/liquidz/antq#clojure-cli-tools-11111139-or-later). Then run one of the following commands:

```bash
# To display outdated dependencies:
clojure -Tantq outdated

# To upgrade outdated dependencies:
clojure -Tantq outdated :upgrade true
# also available as a bb task
bb upgrade

# To upgrade outdated dependencies, including your globally installed tools:
clojure -Tantq outdated :check-clojure-tools true :upgrade true
```

## Adding translation languages

To add a new translation language, create a new file in `resources/locale`. Use the existing translation files as a starting point. Then add the language to `supported-languages` in `src/clj/techdoc/dashboard/config.clj`. Be sure to specify the path to the language file.

## License

Unless noted otherwise in the heading of a given file, all code in this repository is licensed under the terms of Eclipse Public License 2.0 (full text available in the LICENSE file). Licenses and copyright notices for dependencies are available in the DEPENDENCIES.txt file.
